//
//  AppDelegate.h
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "IIViewDeckController.h"
#import "ViewController.h"
#import <FacebookSDK/FacebookSDK.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,FBLoginViewDelegate>

@property (strong, nonatomic) UIWindow *window;
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;
@property(nonatomic, assign) NSInteger mark;
@property(nonatomic, assign) NSNumber* opponantPlayerId;
@property(nonatomic, assign) NSNumber* challangeId;
@property (assign) BOOL locationUseBool;
@property (assign) BOOL challangwithEmail;
@property (assign) BOOL opponantplayerPlay;
@property (assign) BOOL responseTypeInJson;
@property (assign) BOOL loginwithFacebook;
@property (assign) BOOL hidebackButton;
@property (assign) BOOL hidebackButtonforlogin;
@property (assign) NSInteger totalMark;
@property (assign) BOOL menuvalue;
@property (strong, nonatomic) IBOutlet NSArray *loginUser;

@property (strong, nonatomic) IBOutlet NSArray *QuestionArray;
@property (strong, nonatomic) IBOutlet id facebookloginUser;
@property (strong, nonatomic) IBOutlet NSMutableArray *tagArray;
@property (strong, nonatomic) IIViewDeckController* deckController;
@property (strong, nonatomic) IBOutlet NSArray *opponantUser;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@property (weak, nonatomic) IBOutlet UIImage *profileImageForUser;
@end

