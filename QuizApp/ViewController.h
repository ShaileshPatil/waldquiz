//
//  ViewController.h
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
#import "ADSlidingViewController.h"
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface ViewController : GAITrackedViewController<FBSDKLoginButtonDelegate,FBLoginViewDelegate>
//- (IBAction)loginTofacebook:(id)sender;

- (IBAction)loginToemailAction:(id)sender;
- (IBAction)registerAction:(id)sender;
//- (IBAction)SampleActionPerformed:(id)sender;
//- (IBAction)showUserActionPerformed:(id)sender;
//@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *loginActionButton;
@property (weak, nonatomic) IBOutlet UIImageView *forrestImageView;
- (IBAction)LoginButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet FBLoginView *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end

