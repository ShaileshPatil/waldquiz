//
//  ViewController.m
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "FacebookViewController.h"
#import "RegisterViewController.h"
#import "NewGameViewController.h"
#import "RankViewController.h"
#import "UserListViewController.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController ()
{
     AppDelegate *objAppDelegate;
     NSMutableData *receivedData;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    self.loginButton.backgroundColor = [UIColor colorWithRed:65/255.0 green:93/255.0 blue:174/255.0 alpha:1];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
     self.navigationItem.title = @"Start";
     //self.navigationItem.title = @"START";
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
//    self.loginButton.readPermissions = @[@"public_profile", @"email",@"user_friends"];
//    self.loginButton.delegate = self;

    self.loginButton.readPermissions =@[@"public_profile", @"email",@"user_friends"];
    self.loginButton.delegate  =self;
 self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    //
//    FBLoginView *loginview =
//    [[FBLoginView alloc] initWithPermissions:[NSArray arrayWithObject:@"publish_actions"]];
//    
//    
//    loginview.frame = CGRectMake(25, 245, 265, 45);
//    for (id obj in loginview.subviews)
//    {
//        if ([obj isKindOfClass:[UIButton class]])
//        {
//            UIButton * loginButton =  obj;
//            UIImage *loginImage = [UIImage imageNamed:@"facebook_button.png"];
//            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
//            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
//            [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
//            [loginButton sizeToFit];
//        }
//        if ([obj isKindOfClass:[UILabel class]])
//        {
//            UILabel * loginLabel =  obj;
//            loginLabel.text = @"";
//            loginLabel.textAlignment = NSTextAlignmentCenter;
//            loginLabel.frame = CGRectMake(0, 0, 271, 37);
//        }
//    }
//    
//    loginview.delegate = self;
//    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"ViewController";
}
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error
{
    if (error == nil)
    {
        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        [self.navigationController pushViewController:objHomeViewController animated:YES];
        
    }
    
}
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}
-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    NSLog(@"%@", user);
    objAppDelegate.loginwithFacebook = NO;
    objAppDelegate.facebookloginUser = user;
    NSLog(@"%@", objAppDelegate.facebookloginUser);
    [self serverCommunication];
   //    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    
//    [self.navigationController pushViewController:objHomeViewController animated:YES];
    
}
-(void)serverCommunication
{
//    action = fbUserRegistration
//    first_name = Shailendra
//    last_name = Patil
//    name = Shailendra Patil
//    email = shailendra.patil1234@rediff.com
//    password = Patil@123
//    gender = male
//    fb_friend_id = 656066491161264
    
    
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *post = [NSString stringWithFormat:@"first_name=%@&last_name=%@&name=%@&email=%@&gender=%@&fb_friend_id=%@",[objAppDelegate.facebookloginUser valueForKey:@"first_name"],[objAppDelegate.facebookloginUser valueForKey:@"last_name"],[objAppDelegate.facebookloginUser valueForKey:@"name"],[objAppDelegate.facebookloginUser valueForKey:@"email"],[objAppDelegate.facebookloginUser valueForKey:@"gender"],[objAppDelegate.facebookloginUser valueForKey:@"id"]];
        // NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",@"aa@gmail.com",@"123"];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        //[request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=fbUserRegistration"]];
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=fbUserRegistration"]];
    
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    
    
    
}

#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    //    NSError* error;
    //    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
    //                                                         options:kNilOptions
    //                                                           error:&error];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"msg"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Einloggen " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        
        
        NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
        [removeUD removeObjectForKey:@"loginUser"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        NSUserDefaults * removeUD1 = [NSUserDefaults standardUserDefaults];
        [removeUD1 removeObjectForKey:@"userName"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        NSUserDefaults * removeUD2 = [NSUserDefaults standardUserDefaults];
        [removeUD2 removeObjectForKey:@"passWord"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        objAppDelegate.loginUser = json;
        objAppDelegate.profileImageForUser = [[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"image"];
        objAppDelegate.totalMark =[[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"points"]integerValue];
        //                    UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Login " message:@"Benutzer Erfolgreich Einloggen"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //
        //                   //[connectSuccessMessage setTag:1];
        //            [connectSuccessMessage show];
        //        MenuViewController *objMenuViewController= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MenuViewController"];
        //        objMenuViewController.loginUser = array;
        //        [self.navigationController pushViewController:objMenuViewController animated:YES];
        
        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
        //objHomeViewController.loginUser =array;
        //        [self.navigationController pushViewController:objHomeViewController animated:YES];
        //objAppDelegate.hidebackButton = YES;

        UIStoryboard *storiboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *menuViewController = [storiboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
        
        [self.navigationController setNavigationBarHidden:NO];
        self.viewDeckController.rightController = menuViewController;
        
      
    [[self navigationController] setViewControllers:[NSArray arrayWithObject:objHomeViewController]  animated:NO];
        
//        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
//        //objNewGameViewController.loginUser =user;
//        objAppDelegate.hidebackButton = YES;
//        [self.navigationController pushViewController:objHomeViewController animated:YES];
    }
    
    
}

-(void)toggleHiddenState:(BOOL)shouldHide
{
   NSLog(@"toggleHiddenState");
}
-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
     NSLog(@"loginViewShowingLoggedInUser");
   
}
-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
   loginView.backgroundColor = [UIColor blueColor];
    loginView.layer.contents = (id)[UIImage imageNamed:@"facebook_button.png"].CGImage;

     NSLog(@"loginViewShowingLoggedOutUser");
}
-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSLog(@"%@", [error localizedDescription]);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginToemailAction:(id)sender
{
     objAppDelegate.hidebackButtonforlogin = YES;
    NSString *username = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"userName"];
    NSString *password = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"passWord"];
    NSArray *loginUser = [[NSUserDefaults standardUserDefaults]
                          valueForKey:@"loginUser"];
    objAppDelegate.loginUser = loginUser;
    objAppDelegate.profileImageForUser = [[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"image"];
     objAppDelegate.totalMark =[[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"points"]integerValue];
    
    if (username != nil && password != nil)
    {
        
        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        UIStoryboard *storiboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *menuViewController = [storiboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
        
        [self.navigationController setNavigationBarHidden:NO];
        self.viewDeckController.rightController = menuViewController;
        
        
        [[self navigationController] setViewControllers:[NSArray arrayWithObject:objHomeViewController]  animated:NO];

    }
    else
    {
     objAppDelegate.loginwithFacebook = NO;
    LoginViewController *objLoginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objLoginViewController animated:YES];
    }

}

- (IBAction)registerAction:(id)sender
{
     objAppDelegate.loginwithFacebook = NO;
    RegisterViewController *objRegisterViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:objRegisterViewController animated:YES];
}

- (IBAction)LoginButtonAction:(id)sender
{
    objAppDelegate.hidebackButtonforlogin = YES;
    LoginViewController *objLoginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objLoginViewController animated:YES];
    
}
@end
