//
//  FacebookTableViewCell.h
//  QuizApp
//
//  Created by Shailendra on 03/07/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface FacebookTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet FBProfilePictureView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *customNameLabel;

@end
