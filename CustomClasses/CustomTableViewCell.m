//
//  CustomTableViewCell.m
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
