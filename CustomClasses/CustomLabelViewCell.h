//
//  CustomLabelViewCell.h
//  QuizApp
//
//  Created by Shailendra on 04/06/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabelViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *customLabel;

@end
