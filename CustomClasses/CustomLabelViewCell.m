//
//  CustomLabelViewCell.m
//  QuizApp
//
//  Created by Shailendra on 04/06/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "CustomLabelViewCell.h"

@implementation CustomLabelViewCell

- (void)awakeFromNib {
    self.contentView.backgroundColor= [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
