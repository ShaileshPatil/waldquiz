//
//  QuestionWithImageViewController.m
//  QuizApp
//
//  Created by Shailendra on 05/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "QuestionWithImageViewController.h"
#import "InfoViewController.h"
#import "AppDelegate.h"
#import "AsyncImageView.h"

@interface QuestionWithImageViewController ()
{
    NSArray *allquestion;
    NSInteger mark;
    AppDelegate *objAppDelegate;
}


@end

@implementation QuestionWithImageViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     mark =objAppDelegate.mark;
    self.navigationItem.hidesBackButton = YES;
    [self.NextButtonActionPerformed setHidden:YES];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationItem.title = @"Frage 2/3";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];

    UIColor *borderColor = [UIColor colorWithRed:182 green:10 blue:96 alpha:1.0];
    [self.questionImageView.layer setBorderColor:borderColor.CGColor];
    [self.questionImageView.layer setBorderWidth:1.0];
    
//    UIView *contentView;
//    // scrollview won't scroll unless content size explicitly set
//    //
//    
//    //if the contentView is not already inside your scrollview in your xib/StoryBoard doc
//    
//    self.imageScrollView.contentSize = contentView.frame.size;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
      self.screenName = @"QuestionWithImageViewController";
     allquestion = self.questionsArrayWithImage;
    [self.questionLabelText setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];
    self.questionLabelText.text = [[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"question"];
    //[self.Option1buttonClicked setTitle:[[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"option1"] forState:UIControlStateNormal];
    
    self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
 self.NextButtonActionPerformed.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
     
    
    NSString *option1 =[[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"option1"];
    NSInteger len = [option1 length];
    NSLog(@"len:%ld" ,(long)len);
    if (len >=80)
    {
        [self.Option1buttonClicked setFrame:CGRectMake(27, 280, 265, 60)];
    }

    [self.Option1buttonClicked setTitle:option1 forState:UIControlStateNormal];
   
    self.Option1buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.Option1buttonClicked.titleLabel.numberOfLines = 3;
    [self.Option1buttonClicked sizeToFit];
    
    NSString *option2 =[[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"option2"];
    NSInteger len1 = [option2 length];
    NSLog(@"len1:%ld" ,(long)len1);
    if (len1 >=80)
    {
        [self.option2buttonClicked setFrame:CGRectMake(27, 345, 265, 60)];
    }

    
    [self.option2buttonClicked setTitle:option2 forState:UIControlStateNormal];
    self.option2buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.option2buttonClicked.titleLabel.numberOfLines = 3;
    [self.option2buttonClicked sizeToFit];
    NSString *option3 =[[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"option3"];
    NSInteger len2 = [option3 length];
    NSLog(@"len2:%ld" ,(long)len2);
    
    if (len2 >=80)
    {
        [self.option3buttonClicked setFrame:CGRectMake(27, 410, 265, 60)];
    }
    [self.option3buttonClicked setTitle:option3 forState:UIControlStateNormal];
    self.option3buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.option3buttonClicked.titleLabel.numberOfLines = 3;
    [self.option3buttonClicked sizeToFit];
   // self.questionImageView.image= [[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"question_photo"];
     self.questionImageView.imageURL =[NSURL URLWithString:[[self.questionsArrayWithImage objectAtIndex:self.currentIndex]valueForKey:@"question_photo"]];
    NSLog(@"%@",self.questionImageView.imageURL);
    
    
    NSString *titleString =[NSString stringWithFormat:@"Frage %d/3",self.currentIndex+1];
    self.navigationItem.title =titleString;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-  ButtonActionPerformed
- (IBAction)Option1ActionPerformed:(UIButton *)sender
{
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextButtonActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
        objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option2buttonClicked.userInteractionEnabled = NO;
        self.option3buttonClicked.userInteractionEnabled = NO;
    }
    else
    {
        sender.backgroundColor = [UIColor redColor];
        
        UIButton *optionButton2 = ( UIButton *)[self.view viewWithTag:2];
        UIButton *optionButton3 = ( UIButton *)[self.view viewWithTag:3];
        optionButton3.userInteractionEnabled = NO;
        optionButton2.userInteractionEnabled = NO;
        if ([optionButton2.titleLabel.text isEqualToString:answer])
        {
            optionButton2.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton3.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
            
            
        }
        
        else if ([optionButton3.titleLabel.text isEqualToString:answer])
        {
            optionButton3.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton2.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        
        
        
    }

    
}

- (IBAction)Option2ActionPerformed:(UIButton *)sender
{
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextButtonActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
        objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.Option1buttonClicked.userInteractionEnabled = NO;
        self.option3buttonClicked.userInteractionEnabled = NO;
    }
    
    else
    {
        sender.backgroundColor = [UIColor redColor];
        
        UIButton *optionButton1 = ( UIButton *)[self.view viewWithTag:1];
        UIButton *optionButton3 = ( UIButton *)[self.view viewWithTag:3];
        optionButton1.userInteractionEnabled = NO;
        optionButton3.userInteractionEnabled = NO;
        if ([optionButton1.titleLabel.text isEqualToString:answer])
        {
            optionButton1.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton3.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
            
            
        }
        
        else if ([optionButton3.titleLabel.text isEqualToString:answer])
        {
            optionButton3.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton1.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        
        
        
    }

}

- (IBAction)Option3ActionPerformed:(UIButton *)sender
{
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextButtonActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
        objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.Option1buttonClicked.userInteractionEnabled = NO;
        self.option2buttonClicked.userInteractionEnabled = NO;
    }
    else
    {
        sender.backgroundColor = [UIColor redColor];
        
        UIButton *optionButton1 = ( UIButton *)[self.view viewWithTag:1];
        UIButton *optionButton2 = ( UIButton *)[self.view viewWithTag:2];
        optionButton1.userInteractionEnabled = NO;
        optionButton2.userInteractionEnabled = NO;
        
        
        if ([optionButton1.titleLabel.text isEqualToString:answer])
        {
            optionButton1.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton2.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
            
            
        }
        
        else if ([optionButton2.titleLabel.text isEqualToString:answer])
        {
            optionButton2.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton1.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        
        
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)NextButtonClicked:(id)sender
{
    InfoViewController *objInfoViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"InfoViewController"];
    objInfoViewController.currentIndex =self.currentIndex;
    objInfoViewController.questionsArray = self.questionsArrayWithImage;
    //objInfoViewController.loginUser =self.loginUser;
    [self.navigationController pushViewController:objInfoViewController animated:YES];

}
@end
