//
//  QuestionWithImageViewController.h
//  QuizApp
//
//  Created by Shailendra on 05/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "GAITrackedViewController.h"

@interface QuestionWithImageViewController : GAITrackedViewController

@property (strong, nonatomic) IBOutlet AsyncImageView *questionImageView;
@property (strong, nonatomic) IBOutlet UIButton *Option1buttonClicked;
@property (strong, nonatomic) IBOutlet UIButton *option2buttonClicked;
@property (strong, nonatomic) IBOutlet UIButton *option3buttonClicked;
- (IBAction)Option1ActionPerformed:(id)sender;
- (IBAction)Option2ActionPerformed:(id)sender;
- (IBAction)Option3ActionPerformed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *NextButtonActionPerformed;

- (IBAction)NextButtonClicked:(id)sender;
@property(nonatomic, assign) int currentIndex;
@property (strong, nonatomic) IBOutlet NSArray *questionsArrayWithImage;
@property (weak, nonatomic) IBOutlet UITextView *questionLabelText;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;

@end
