//
//  FacebookFriendListViewController.h
//  QuizApp
//
//  Created by Shailendra on 26/06/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "GAITrackedViewController.h"


@interface FacebookFriendListViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,FBSDKGameRequestDialogDelegate,FBSDKAppInviteDialogDelegate>
@property (weak, nonatomic) IBOutlet UITableView *facebookFriendList;
@property (strong, nonatomic) IBOutlet NSArray *FacebbolFriendList;
@end
