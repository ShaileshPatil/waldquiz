//
//  FacebookFriendListViewController.m
//  QuizApp
//
//  Created by Shailendra on 26/06/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "FacebookFriendListViewController.h"
#import "CustomTableViewCell.h"
#import "AsyncImageView.h"
#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AppDelegate.h"
#import "MultiplePlayerViewController.h"
#import "FacebookTableViewCell.h"

@interface FacebookFriendListViewController ()
{
    AppDelegate *objAppDelegate;

}

@end

@implementation FacebookFriendListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    [self.facebookFriendList setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];
    self.navigationItem.title = @"Facebook friend ist";
    //self.navigationItem.title = @"FACEBOOK FRIEND IST";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    
                                                                    };
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
     [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    self.screenName = @"FacebookFriendListViewController";
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
}
-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [self.FacebbolFriendList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    //3
    FacebookTableViewCell *cell = (FacebookTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"FacebookTableViewCell" owner:self options:nil];
        cell=(FacebookTableViewCell *) [nibbb objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
        cell.customNameLabel.text =[[self.FacebbolFriendList objectAtIndex:indexPath.section] valueForKey:@"name"];
         //NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
    NSLog(@"ID%@",[[self.FacebbolFriendList objectAtIndex:indexPath.section] valueForKey:@"id"]);
        cell.profilePicture.profileID = [[self.FacebbolFriendList objectAtIndex:indexPath.section] valueForKey:@"id"];

    
    //cell.cityNameLbl.text = [NSString stringWithFormat:@"Day %d",i];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog( @"### running FB sdk version: %@", [FBSettings sdkVersion] );
    NSArray *selectedobject = [self.FacebbolFriendList objectAtIndex:indexPath.section];
    NSString *facebookID =[[self.FacebbolFriendList objectAtIndex:indexPath.section] valueForKey:@"id"];

    NSMutableDictionary* params =
    [NSMutableDictionary dictionaryWithObject:facebookID forKey:@"to"];
    
    NSString *message = @"Request send by WaldQuizApp";
    NSString *title = @"WaldQuizApp";
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:message
                                                    title:title
                                               parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                   if (error)
                                                   {
                                                       // Case A: Error launching the dialog or sending request.
                                                       NSLog(@"Error sending request.");
                                                   }
                                                   else
                                                   {
                                                       if (result == FBWebDialogResultDialogNotCompleted)
                                                       {
                                                           // Case B: User clicked the "x" icon
                                                           NSLog(@"User canceled request.");
                                                       }
                                                       else
                                                       {
                                                           NSLog(@"Request Sent. %@", params);
                                                           
                                                           
                                                           
                                                           objAppDelegate.opponantplayerPlay = NO;
                                                           MultiplePlayerViewController *objMultiplePlayerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MultiplePlayerViewController"];
                                                           objAppDelegate.loginwithFacebook = YES;
                                                           objAppDelegate.opponantUser =selectedobject;
                                                           objMultiplePlayerViewController.currentIndex = 0;
                                                           [self.navigationController pushViewController:objMultiplePlayerViewController animated:YES];

                                                       }
                                                   }}];
}

@end
