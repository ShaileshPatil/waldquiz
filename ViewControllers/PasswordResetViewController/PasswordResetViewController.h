//
//  PasswordResetViewController.h
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PasswordResetViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)resetPasswordActionPerformed:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *passwordresetwebview;

@end
