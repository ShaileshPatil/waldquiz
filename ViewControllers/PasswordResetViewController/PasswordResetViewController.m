//
//  PasswordResetViewController.m
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "PasswordResetViewController.h"

@interface PasswordResetViewController ()

@end

@implementation PasswordResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Passwort vergessen";
    //self.navigationItem.title = @"PASSWORT VERGESSEN";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor = self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    self.passwordresetwebview.backgroundColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    NSString *urlAddress = @"http://www.waldquiz.ch/login/?tx_felogin_pi1[forgot]=1&no_navigation=1";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [self.passwordresetwebview loadRequest:requestObj];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"PasswordResetViewController";
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
}
-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resetPasswordActionPerformed:(id)sender
{
    
}
@end
