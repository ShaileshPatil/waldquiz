//
//  UserListViewController.m
//  QuizApp
//
//  Created by Shailendra on 17/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "UserListViewController.h"
#import "UserRecordsViewController.h"

@interface UserListViewController ()
{
    NSMutableData *receivedData;
    NSMutableArray  *idofUser;
}

@end

@implementation UserListViewController

#pragma mark-View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    idofUser= [[NSMutableArray alloc]init];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"UserList";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 24);
    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    
    [closeButton addTarget:self action:@selector(closenew) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
//     [self serverCommunication];
    [super viewWillAppear:animated];
    self.screenName = @"UserListViewController";
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)closenew
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}
#pragma mark-  textFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    //[self.Scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableViewDatasourse Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [idofUser count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
    {
        
        
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
   

    cell.textLabel.text = [NSString stringWithFormat:@"U_ID:           %@",[[idofUser objectAtIndex:indexPath.row] valueForKey:@"uid"]];

    return cell;
    
}
#pragma mark-TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserRecordsViewController *objUserRecordsViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"UserRecordsViewController"];
    objUserRecordsViewController.Dict =[idofUser objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:objUserRecordsViewController animated:YES];
    
}

#pragma mark-Private Methods
- (IBAction)getUserListActionPerformed:(id)sender
{
     [self serverCommunication];
}
-(void)serverCommunication
{


    NSString *postLength;
    NSData *postData;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    if (![self.userIdTextField.text isEqualToString:@""])
    {
        NSString *post = [NSString stringWithFormat:@"user_id=%@",self.userIdTextField.text];
        postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getUserById"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
    }
    
    else
    {
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getUsers"]];
    }
    
    
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        NSLog(@"Connection Successful");
    }
    else
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }

}
    

#pragma mark-Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:kNilOptions
                                                           error:&error];
    
  
    
   
    if ([json isKindOfClass:[NSDictionary class]])
    {
         NSString* responseError = [json valueForKey:@"error"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        [idofUser removeAllObjects];
        for (NSDictionary *dict in json )
        {
            [idofUser addObject:dict];
        }
        if ([json count] == 1)
        {
            
            UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Benutzer sind verfügbar"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectSuccessMessage setTag:1];
            [connectSuccessMessage show];
        }
        else
        {
            UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Dies alles Benutzer Name und ID"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectSuccessMessage setTag:1];
            [connectSuccessMessage show];
            
            
        }

       
       
        
    }
}


    
    

#pragma mark-AlertView Delegate Methods6
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Cancel Tapped.");
            [self.userListTableView reloadData];
            
        }
        
    }
}


@end
