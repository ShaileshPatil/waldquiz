//
//  UserListViewController.h
//  QuizApp
//
//  Created by Shailendra on 17/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface UserListViewController : GAITrackedViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

- (IBAction)getUserListActionPerformed:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *userListTableView;
@property (strong, nonatomic) IBOutlet UITextField *userIdTextField;

@end
