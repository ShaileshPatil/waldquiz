//
//  MCQViewController.h
//  QuizApp
//
//  Created by Shailendra on 22/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCQViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *quelabel;

@property (strong, nonatomic) IBOutlet UIButton *option1ButtonClicked;

@property (strong, nonatomic) IBOutlet UIButton *option2ButtonClicked;
@property (strong, nonatomic) IBOutlet UIButton *option3ButtonClicked;
- (IBAction)OptionClickActionPerformed:(id)sender;
- (IBAction)Option2ClickActionPerformed:(id)sender;
- (IBAction)Option3ClickActionPerformed:(id)sender;

@end
