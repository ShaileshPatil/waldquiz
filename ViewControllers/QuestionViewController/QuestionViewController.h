//
//  QuestionViewController.h
//  QuizApp
//
//  Created by Shailendra on 22/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface QuestionViewController : GAITrackedViewController


@property (strong, nonatomic) IBOutlet UIButton *Option1buttonClicked;
@property (strong, nonatomic) IBOutlet UIButton *option2buttonClicked;
@property (strong, nonatomic) IBOutlet UIButton *option3buttonClicked;
- (IBAction)Option1ActionPerformed:(id)sender;
- (IBAction)Option2ActionPerformed:(id)sender;
- (IBAction)Option3ActionPerformed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *NextActionPerformed;
- (IBAction)NextButtonClicked:(id)sender;
@property(nonatomic, assign) int currentIndex;

@property (weak, nonatomic) IBOutlet UITextView *questionLabelText;
@property (strong, nonatomic) IBOutlet NSArray *questionsArray;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@end
