//
//  QuestionViewController.m
//  QuizApp
//
//  Created by Shailendra on 22/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "QuestionViewController.h"
#import "InfoViewController.h"
#import "AppDelegate.h"

@interface QuestionViewController ()
{
    NSArray *allquestion;
    NSInteger mark;
    AppDelegate *objAppDelegate;
}

@end

@implementation QuestionViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
  
    mark = objAppDelegate.mark;
    [self.NextActionPerformed setHidden:YES];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    allquestion = self.questionsArray;
//self.navigationItem.title = @"Frage 2/3";
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.screenName = @"QuestionViewController";
    [self.questionLabelText setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];
    self.questionLabelText.text = [[allquestion objectAtIndex:self.currentIndex]valueForKey:@"question"];
    
    self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
     self.NextActionPerformed.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    NSString *option1 =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"option1"];
    NSInteger len = [option1 length];
    NSLog(@"len:%ld" ,(long)len);
    if (len >=80)
    {
        [self.Option1buttonClicked setFrame:CGRectMake(24, 230, 265, 60)];
    }

    
    
   [self.Option1buttonClicked setTitle:option1 forState:UIControlStateNormal];
    self.Option1buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.Option1buttonClicked.titleLabel.numberOfLines = 3;
//    [self.Option1buttonClicked sizeToFit];
    
    NSString *option2 =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"option2"];
    NSInteger len1 = [option2 length];
    NSLog(@"len1:%ld" ,(long)len1);
    if (len1 >=80)
    {
        [self.option2buttonClicked setFrame:CGRectMake(24, 310, 265, 60)];
    }


    [self.option2buttonClicked setTitle:option2 forState:UIControlStateNormal];
    self.option2buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.option2buttonClicked.titleLabel.numberOfLines = 3;
// [self.option2buttonClicked sizeToFit];
    
    NSString *option3 =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"option3"];
    NSInteger len2 = [option2 length];
    NSLog(@"len2:%ld" ,(long)len2);
    if (len2 >=80)
    {
        [self.option3buttonClicked setFrame:CGRectMake(24, 380, 265, 60)];
    }
    [self.option3buttonClicked setTitle:option3 forState:UIControlStateNormal];
    self.option3buttonClicked.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.option3buttonClicked.titleLabel.numberOfLines = 3;
//    [self.option3buttonClicked sizeToFit];
    NSString *titleString = [NSString stringWithFormat:@"Frage %d/3",self.currentIndex+1];
    self.navigationItem.title =titleString;


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-  ButtonActionMethods
- (IBAction)Option1ActionPerformed:(UIButton *)sender
{
   
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
        
        objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option2buttonClicked.userInteractionEnabled = NO;
        self.option3buttonClicked.userInteractionEnabled = NO;
    }
    else{
        sender.backgroundColor = [UIColor redColor];

        UIButton *optionButton2 = ( UIButton *)[self.view viewWithTag:2];
        UIButton *optionButton3 = ( UIButton *)[self.view viewWithTag:3];
        optionButton3.userInteractionEnabled = NO;
        optionButton2.userInteractionEnabled = NO;
        if ([optionButton2.titleLabel.text isEqualToString:answer])
        {
            optionButton2.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton3.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];

            
        }
        
        else if ([optionButton3.titleLabel.text isEqualToString:answer])
        {
            optionButton3.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton2.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        

        
    }
}


- (IBAction)Option2ActionPerformed:(UIButton *)sender
{
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
          objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option3buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.Option1buttonClicked.userInteractionEnabled = NO;
        self.option3buttonClicked.userInteractionEnabled = NO;
    }
    
    else
    {
        sender.backgroundColor = [UIColor redColor];
        
        UIButton *optionButton1 = ( UIButton *)[self.view viewWithTag:1];
        UIButton *optionButton3 = ( UIButton *)[self.view viewWithTag:3];
        optionButton1.userInteractionEnabled = NO;
        optionButton3.userInteractionEnabled = NO;
        if ([optionButton1.titleLabel.text isEqualToString:answer])
        {
            optionButton1.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton3.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
            
            
        }
        
        else if ([optionButton3.titleLabel.text isEqualToString:answer])
        {
            optionButton3.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton1.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        
        
        
    }
}

- (IBAction)Option3ActionPerformed:(UIButton *)sender
{
    [objAppDelegate.tagArray addObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    [self.NextActionPerformed setHidden:NO];
    NSString *answer =[[allquestion objectAtIndex:self.currentIndex]valueForKey:@"correct_answer_text"];
    if ([sender.titleLabel.text isEqualToString:answer])
    {
          objAppDelegate.mark = mark +1;
        sender.backgroundColor = [UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
        self.Option1buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.option2buttonClicked.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        self.Option1buttonClicked.userInteractionEnabled = NO;
        self.option2buttonClicked.userInteractionEnabled = NO;
    }
    else
    {
        sender.backgroundColor = [UIColor redColor];
        
        UIButton *optionButton1 = ( UIButton *)[self.view viewWithTag:1];
        UIButton *optionButton2 = ( UIButton *)[self.view viewWithTag:2];
        optionButton1.userInteractionEnabled = NO;
        optionButton2.userInteractionEnabled = NO;

        
        if ([optionButton1.titleLabel.text isEqualToString:answer])
        {
            optionButton1.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton2.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
            
            
        }
        
        else if ([optionButton2.titleLabel.text isEqualToString:answer])
        {
            optionButton2.backgroundColor =[UIColor colorWithRed:0/255.0 green:143/255.0 blue:41/255.0 alpha:1];
            optionButton1.backgroundColor = [UIColor colorWithRed:212/255.0 green:130/255.0 blue:91/255.0 alpha:1];
        }
        
        
        
    }
    
}
- (IBAction)NextButtonClicked:(UIButton *)sender
{
    InfoViewController *objInfoViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"InfoViewController"];
    objInfoViewController.currentIndex =self.currentIndex;
    objInfoViewController.questionsArray = self.questionsArray;
    //objInfoViewController.loginUser= self.loginUser;
    NSLog(@"%@",objAppDelegate.opponantPlayerId);
    
    [self.navigationController pushViewController:objInfoViewController animated:YES];
   
    
}
@end
