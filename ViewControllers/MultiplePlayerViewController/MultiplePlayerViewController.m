//
//  MultiplePlayerViewController.m
//  QuizApp
//
//  Created by Shailendra on 29/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "MultiplePlayerViewController.h"
//#import "Base64.h"
#import "QuestionWithImageViewController.h"
#import "QuestionViewController.h"
#import "AppDelegate.h"


@interface MultiplePlayerViewController ()
{
    NSMutableData *receivedData;
    NSArray *array;
    NSArray *CurrentPlayerInfo;
    int UserId;
    NSString *string;
    NSArray *questionArray;
     UIViewController *nextView;
     AppDelegate *objAppDelegate;
   
}

@end

@implementation MultiplePlayerViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad

{
    [super viewDidLoad];
   
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Neues Spiel";
     //self.navigationItem.title = @"NEUES SPIEL";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

   
    CurrentPlayerInfo=objAppDelegate.loginUser ;
    questionArray= objAppDelegate.QuestionArray;
    if (objAppDelegate.loginwithFacebook == YES)
    {
       UserId =[[[CurrentPlayerInfo objectAtIndex:0]valueForKey:@"uid"] intValue];
    }
    else
    {
    
        UserId =[[[CurrentPlayerInfo objectAtIndex:0]valueForKey:@"uid"] intValue];
        string = [NSString stringWithFormat:@"%d", UserId];
   
    
   
     [self serverCommunication];
    }
//    [self serverCommunication];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"MultiplePlayerViewController";
    if (nextView != nil)
    {
        _currentIndex ++;
        if ([[[questionArray objectAtIndex:_currentIndex]valueForKey:@"question_photo"]isEqualToString:@""] )
        {
            QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
            objQuestionViewController.questionsArray =questionArray;
            objQuestionViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionViewController animated:YES];
            
        }
        else
        {
            QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
            objQuestionWithImageViewController.questionsArrayWithImage =questionArray;
            objQuestionWithImageViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
            
            
        }
    }

       
    if ([[[CurrentPlayerInfo objectAtIndex:0]valueForKey:@"image"]isEqualToString:@""])
    {
        //stself.firstPlayerImageView.image=[UIImage imageNamed:@"chat.png"] ;
        
        NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
        self.firstPlayerImageView.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
    }
    else
    {
        
       self.firstPlayerImageView.imageURL = [NSURL URLWithString:(NSString *)[[CurrentPlayerInfo objectAtIndex:0] valueForKey:@"image"]];
           }

   
    self.firstPlayerNameLabel.text=[[CurrentPlayerInfo objectAtIndex:0]valueForKey:@"first_name"];
    
    if (objAppDelegate.loginwithFacebook == YES)
    {
        NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
        self.secondPlayerImageView.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
        self.secondPlayerName.text= [objAppDelegate.opponantUser valueForKey:@"name"];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:[objAppDelegate.opponantUser valueForKey:@"id"]];
        objAppDelegate.opponantPlayerId = myNumber;

    }
  
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
}

-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

    


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-  connectionMethods
-(void)serverCommunication
{
        NSString *post = [NSString stringWithFormat:@"user_id=%@",string];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getRandomUserId"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    
   }
#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"msg"];
        
        if (responseError ==nil)
        {
            //objAppDelegate.responseTypeInJson = YES;
            objAppDelegate.opponantUser = json;
           
            if ([[json valueForKey:@"image"]isEqualToString:@""])
            {
                //self.secondPlayerImageView.image=[UIImage imageNamed:@"chat.png"] ;
                
                NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
                self.secondPlayerImageView.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
                
            }
            else
            {
             //self.firstPlayerImageView.imageURL = [NSURL URLWithString:(NSString *)[[CurrentPlayerInfo objectAtIndex:0] valueForKey:@"image"]];
                self.secondPlayerImageView.imageURL= [NSURL URLWithString:(NSString *)[json valueForKey:@"image"]];
            }
            self.secondPlayerName.text= [json valueForKey:@"name"];
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:[json valueForKey:@"uid"]];
             objAppDelegate.opponantPlayerId = myNumber;

        }
        else
        {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        
        [connectFailMessage show];
        }
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        array = json;
        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Benutzer erfolgreich Einloggen"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [connectSuccessMessage setTag:1];
       
        [connectSuccessMessage show];
        
        
        
    }
    
    
}


#pragma mark-  ButtonActionMethods
- (IBAction)furtherButtonAction:(id)sender
{
//    if ([[[self.questionArray objectAtIndex:0]valueForKey:@"image"]isEqualToString:@""])
//    {
//        QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
//        objQuestionViewController.questionsArray =self.questionArray;
//        [self.navigationController pushViewController:objQuestionViewController animated:YES];
//        
//    }
//    else
//    {
//        QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
//        objQuestionWithImageViewController.questionsArrayWithImage =self.questionArray;
//        [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
//        
//        
//    }
    _currentIndex = 0;
    if ([[[questionArray objectAtIndex:0]valueForKey:@"question_photo"]isEqualToString:@""])
    {
        QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
        objQuestionViewController.questionsArray =questionArray;
        objQuestionViewController.currentIndex = 0;
        nextView = objQuestionViewController;
        [self.navigationController pushViewController:objQuestionViewController animated:YES];
        
    }
    else
    {
        QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
        objQuestionWithImageViewController.questionsArrayWithImage =questionArray;
        objQuestionWithImageViewController.currentIndex = 0;
        nextView = objQuestionWithImageViewController;
        [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
        
        
    }

    
}
@end
