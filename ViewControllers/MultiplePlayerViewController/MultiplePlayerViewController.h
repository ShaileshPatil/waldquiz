//
//  MultiplePlayerViewController.h
//  QuizApp
//
//  Created by Shailendra on 29/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface MultiplePlayerViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (strong, nonatomic) IBOutlet NSArray *questionArray;
@property (weak, nonatomic) IBOutlet UIView *firstPlayerView;
@property (weak, nonatomic) IBOutlet UIImageView *firstPlayerImageView;
@property (weak, nonatomic) IBOutlet UILabel *firstPlayerNameLabel;
- (IBAction)furtherButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *secondPlayerImageView;
@property (weak, nonatomic) IBOutlet UILabel *secondPlayerName;

@property (weak, nonatomic) IBOutlet UIView *secondPlayerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;


@property(nonatomic, assign) int currentIndex;


@end
