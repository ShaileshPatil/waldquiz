//
//  PrivatePolicyViewController.h
//  QuizApp
//
//  Created by Shailendra on 27/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"

@interface PrivatePolicyViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIWebView *privatePolicyWebview;

@end
