//
//  TermsViewController.m
//  QuizApp
//
//  Created by Shailendra on 27/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:NO];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    self.view.backgroundColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    
    
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"AGB";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
     self.view.backgroundColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    self.termsWebView.tintColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 24);
    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    
    //    [closeButton addTarget:self action:@selector(closenew:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self.viewDeckController action:@selector(toggleRightView)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;


    
//    1.Instructions, http://www.waldquiz.ch/spielanleitung/
//    2.Terms,  http://www.waldquiz.ch/agb/
//    3.About, http://www.waldquiz.ch/impressum/
//    4.Privacy policy  http://www.waldquiz.ch/datenschutz/
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"terms_agb" withExtension:@"html"];
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSURL *baseUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    [self.termsWebView loadHTMLString:html baseURL:baseUrl];    //Load the request in the UIWebView.
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"TermsViewController";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
