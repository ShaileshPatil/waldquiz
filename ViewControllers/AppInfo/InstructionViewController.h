//
//  InstructionViewController.h
//  QuizApp
//
//  Created by Shailendra on 27/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"

@interface InstructionViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIWebView *instructionWebview;
- (IBAction)newGameButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *newgamweactionPerformed;

@end
