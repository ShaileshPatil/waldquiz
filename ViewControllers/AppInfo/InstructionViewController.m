//
//  InstructionViewController.m
//  QuizApp
//
//  Created by Shailendra on 27/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "InstructionViewController.h"
#import "NewGameViewController.h"


@interface InstructionViewController ()

@end

@implementation InstructionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self.navigationController.navigationBar setHidden:NO];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    self.view.backgroundColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    self.instructionWebview.backgroundColor = [UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    self.newgamweactionPerformed.backgroundColor = [UIColor colorWithRed:15/255.0 green:121/255.0 blue:66/255.0 alpha:1];
      self.instructionWebview.opaque=NO;
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationItem.title = @"SPIELANLEITUNG";
    self.navigationItem.title = @"Spielanleitung";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
   [self.newgamweactionPerformed  setTitle:@"JETZT SPIELEN" forState:UIControlStateNormal];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 24);
    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    
    //    [closeButton addTarget:self action:@selector(closenew:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self.viewDeckController action:@selector(toggleRightView)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;
    //user = objAppDelegate.loginUser;

    NSURL *url = [[NSBundle mainBundle] URLForResource:@"instuction" withExtension:@"html"];
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSURL *baseUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    [self.instructionWebview loadHTMLString:html baseURL:baseUrl];    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"InstructionViewController";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)newGameButtonClicked:(id)sender
{
    NewGameViewController *objNewGameViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"NewGameViewController"];
    //objNewGameViewController.loginUser =user;
    [self.navigationController pushViewController:objNewGameViewController animated:YES];

    
}
@end
