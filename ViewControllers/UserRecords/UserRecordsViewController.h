//
//  UserRecordsViewController.h
//  QuizApp
//
//  Created by Shailendra on 17/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FacebookSDK/FacebookSDK.h>
#import "GAITrackedViewController.h"

@interface UserRecordsViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet NSDictionary *Dict;
@property (strong, nonatomic) IBOutlet UITextField *uIdtextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *pointsTextField;
- (IBAction)updateRecordsAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastnameTextField;
@property (strong, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *updateButton;


@end
