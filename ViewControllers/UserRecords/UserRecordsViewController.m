//
//  UserRecordsViewController.m
//  QuizApp
//
//  Created by Shailendra on 17/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "UserRecordsViewController.h"

@interface UserRecordsViewController ()
{
     BOOL final;
     NSMutableData *receivedData;
}

@end

@implementation UserRecordsViewController

#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.firstNameLabel.hidden=YES;
    self.lastNameLabel.hidden=YES;
    self.firstNameTextField.hidden=YES;
    self.lastnameTextField.hidden =YES;
    self.updateButton.userInteractionEnabled=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"UserRecord";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 30);
//    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    [closeButton setTitle:@"Update" forState:normal];
    
    [closeButton addTarget:self action:@selector(closenew) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;
    
    
    self.uIdtextField.text = [self.Dict valueForKey:@"uid"];
    self.nameTextField.text =[self.Dict valueForKey:@"first_name"];
    self.emailTextField.text=[self.Dict valueForKey:@"email"];
    self.pointsTextField.text=[self.Dict valueForKey:@"points"];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"UserRecordsViewController";
}
#pragma mark-  customMethods
-(BOOL)email:(NSString *)mail
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:mail];
    return YES;
    
    
    //    return [self.emailTextField.text containsString:@"redbytes.in"];
}
#pragma mark-  textFieldDelegateMethods
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    BOOL succes;
    succes=[self email:self.emailTextField.text];
    if (succes==NO)
    {
        
        
        if (self.emailTextField.text.length>0)
        {
            //[self.emailErrorLabel setHidden:FALSE];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte tippen Sie korrekte E-Mail-" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"stornieren", nil];
            [alert show];
        }
        self.emailTextField.text=@"";
        final=NO;
    }
    else
    {
        //[self.emailErrorLabel setHidden:TRUE];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    //[self.Scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}
-(void)tapAction:(UITapGestureRecognizer *)tap
{
    UITextField *textFiled1 =(UITextField*) [self.view viewWithTag:1];
    [textFiled1 resignFirstResponder];
    UITextField *textFiled2 =(UITextField*) [self.view viewWithTag:2];
    [textFiled2 resignFirstResponder];
    UITextField *textFiled3 =(UITextField*) [self.view viewWithTag:3];
    
    [textFiled3 resignFirstResponder];
    UITextView *textview4 =(UITextView*)[self.view viewWithTag:4];
    [textview4 resignFirstResponder];
    [self.view removeGestureRecognizer:tap];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    //[self.Scrollview setContentOffset:CGPointMake(0,165) animated:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(tapAction:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tap];
    return YES;
}


#pragma mark-  customMethods
-(IBAction)closenew
{
    self.firstNameLabel.hidden=NO;
    self.lastNameLabel.hidden=NO;
    self.firstNameTextField.hidden=NO;
    self.lastnameTextField.hidden =NO;
    self.updateButton.userInteractionEnabled=YES;

//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-  connectionMethods
- (IBAction)updateRecordsAction:(id)sender
{
     [self serverCommunication];
    
}
-(void)serverCommunication
{
    
    
    if (self.uIdtextField.text.length>0&&self.firstNameTextField.text .length>0&&self.emailTextField.text.length>0&&self.lastnameTextField.text.length>0)
    {
        NSString *post = [NSString stringWithFormat:@"user_id=%@&first_name=%@&last_name=%@&email=%@",self.uIdtextField.text,self.firstNameTextField.text,self.lastnameTextField.text,self.emailTextField.text];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=editUser"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@" Please enter Required fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:kNilOptions
                                                           error:&error];
    NSString* responseError = [json valueForKey:@"error"];
    
    NSLog(@"error: %@", responseError);
    
    if ([responseError length] > 0 )
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    else
    {
        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Benutzer erfolgreich aktualisiert"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectSuccessMessage setTag:1];
        [connectSuccessMessage show];
        
        
    }
    
    
    
}
#pragma mark-  alertViewDelegateMethods
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Cancel Tapped.");
                        
        }
        
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"response data with Failure- %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Unsecccessfully registriert" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
    [alert show];
    
}


@end
