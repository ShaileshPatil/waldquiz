//
//  SettingViewController.h
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"

@interface SettingViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIButton *savesettingsButton;
- (IBAction)savesettingActionPerformed:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;


@end
