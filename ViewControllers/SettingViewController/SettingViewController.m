//
//  SettingViewController.m
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "SettingViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface SettingViewController ()
{
     BOOL final;
     AppDelegate *objAppDelegate;
     NSString *User_id;
    NSMutableData *receivedData;

}

@end

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Einstellungen";
   // self.navigationItem.title = @"EINSTELLUNGEN";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

//    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    
//    closeButton.frame = CGRectMake(0, 0, 30, 24);
//    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
//    
//    //    [closeButton addTarget:self action:@selector(closenew:) forControlEvents:UIControlEventTouchUpInside];
//    [closeButton addTarget:self.viewDeckController action:@selector(toggleRightView)
//          forControlEvents:UIControlEventTouchUpInside];
    
    //UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    //self.navigationItem.rightBarButtonItem = btnclose;
     User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];
    self.emailTextField.placeholder =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"email"];
    self.nameTextField.placeholder =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"first_name"];
    self.surnameTextField.placeholder = [[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"last_name"];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"SettingViewController";
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self.viewDeckController action:@selector(toggleRightView)];
    
    self.navigationItem.leftBarButtonItem = anotherButton;
}
//-(void)refreshPropertyList
//{
//    [self.navigationController popViewControllerAnimated:YES];
//    
//}

    
    

-(BOOL)email:(NSString *)mail
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:mail];
    return YES;
    
    
    //    return [self.emailTextField.text containsString:@"redbytes.in"];
}

#pragma mark-  textFieldDelegateMethods
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
        return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
   
    return YES;
}
-(void)tapAction:(UITapGestureRecognizer *)tap
{
    UITextField *textFiled1 =(UITextField*) [self.view viewWithTag:1];
    [textFiled1 resignFirstResponder];
    UITextField *textFiled2 =(UITextField*) [self.view viewWithTag:2];
    [textFiled2 resignFirstResponder];
    UITextField *textFiled3 =(UITextField*) [self.view viewWithTag:3];
    
    [textFiled3 resignFirstResponder];
      
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(tapAction:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tap];
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)savesettingActionPerformed:(id)sender
{
    BOOL succes;
    succes=[self email:self.emailTextField.text];
    if (succes==NO)
    {
        
        self.emailTextField.placeholder=@"Email-id";
        if (self.emailTextField.text.length>0)
        {
            //[self.emailErrorLabel setHidden:FALSE];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte tippen Sie korrekte E-Mail-" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
            [alert show];
            self.emailTextField.text=@"";

        }
        else
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte geben Sie alle Felder aus" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        }
        final=NO;
    }
    else
    {
         [self serverCommunication];
    }
}
-(void)serverCommunication
{
    if (self.nameTextField.text.length>0&&self.surnameTextField.text .length>0&&self.emailTextField.text.length>0)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *post = [NSString stringWithFormat:@"user_id=%@&first_name=%@&last_name=%@&email=%@",User_id,self.nameTextField.text,self.surnameTextField.text,self.emailTextField.text];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=editUser"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte geben Sie alle Felder aus" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    
    
}


    #pragma mark-  connectionDelegateMethods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    //    NSError* error;
    //    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
    //                                                         options:kNilOptions
    //                                                           error:&error];
    
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSString* responseError = [json valueForKey:@"error"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
      
        NSArray *array = json;
          NSLog(@"NSArray%@",array);
        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Profile Erfolgreich aktualisiert"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [connectSuccessMessage show];
        self.emailTextField.text=@"";
        self.nameTextField.text=@"";
        self.surnameTextField.text=@"";
        self.emailTextField.placeholder =[[json objectAtIndex:0]valueForKey:@"email"];
        self.nameTextField.placeholder =[[json objectAtIndex:0]valueForKey:@"first_name"];
        self.surnameTextField.placeholder = [[json objectAtIndex:0]valueForKey:@"last_name"];
       
    }
    
    
}


@end
