//
//  LoginViewController.h
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"

@interface LoginViewController : GAITrackedViewController<NSURLConnectionDataDelegate,NSURLConnectionDelegate,UITextFieldDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButtonClicked;
- (IBAction)loginButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *forgotButtonClicked;
- (IBAction)forgotButtonAction:(id)sender;

@end
