//
//  LoginViewController.m
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "LoginViewController.h"
#import "PasswordResetViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "ViewController1.h"

@interface LoginViewController ()
{
    NSMutableData *receivedData;
    NSArray *array;
     AppDelegate *objAppDelegate;
}

@end

@implementation LoginViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    [self.userNameTextField.layer setCornerRadius:10.0f];
    self.userNameTextField.placeholder = @"E-Mail Adresse";
    
    self.passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    [self.passwordTextField.layer setCornerRadius:10.0f];
    
    
    [self.forgotButtonClicked.layer setCornerRadius:10.0f];

    self.passwordTextField.placeholder = @"Passwort";

     self.loginButtonClicked.backgroundColor =[UIColor colorWithRed:15/255.0 green:121/255.0 blue:66/255.0 alpha:1];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationItem.title = @"LOGIN";
    self.navigationItem.title = @"Login";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"LoginViewController";
    if (objAppDelegate.hidebackButtonforlogin == YES)
    {
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                         target:self action:@selector(refreshPropertyList)];
        self.navigationItem.leftBarButtonItem = anotherButton;
    }
    
}
-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark-  textFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    //[self.Scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)loginButtonAction:(id)sender
{
    
    [self serverCommunication];
//    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    [self.navigationController pushViewController:objHomeViewController animated:YES];

 
    
}
#pragma mark-  connectionMethods
-(void)serverCommunication
{
    
if (self.userNameTextField.text.length>0&&self.passwordTextField.text .length>0)
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",self.userNameTextField.text,self.passwordTextField.text];
    // NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",@"aa@gmail.com",@"123"];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=login"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        NSLog(@"Connection Successful");
    }
    else
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }
    
}
else
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hinweis" message:@"Bitte füllen Sie alle Felder aus" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

}

#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
//    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
//                                                         options:kNilOptions
//                                                           error:&error];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"msg"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Einloggen " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        array = json;
        
        [[NSUserDefaults standardUserDefaults] setObject:self.userNameTextField.text forKey:@"userName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.passwordTextField.text forKey:@"passWord"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:json forKey:@"loginUser"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        objAppDelegate.loginUser = json;
         objAppDelegate.totalMark =[[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"points"]integerValue];

        
        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
        //objHomeViewController.loginUser =array;
//        [self.navigationController pushViewController:objHomeViewController animated:YES];
        UIStoryboard *storiboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *menuViewController = [storiboard instantiateViewControllerWithIdentifier:@"MenuViewController"];

        [self.navigationController setNavigationBarHidden:NO];
        self.viewDeckController.rightController = menuViewController;

//        ViewController1 *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"ViewController1"];
//        //objHomeViewController.loginUser =array;
//        [self.navigationController pushViewController:objHomeViewController animated:YES];
        [[self navigationController] setViewControllers:[NSArray arrayWithObject:objHomeViewController]  animated:NO];

        }
        
    
}
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag==1)
//    {
//        if (buttonIndex == 0)
//        {
//            NSLog(@"Cancel Tapped.");
//            [NSTimer scheduledTimerWithTimeInterval:2.0
//                                             target:self
//                                           selector:@selector(targetMethod)
//                                           userInfo:nil
//                                            repeats:NO];
//            
//        }
//        
//    }
//    
//}
//-(void)targetMethod
//{
//    MenuViewController *objMenuViewController= [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MenuViewController"];
//    objMenuViewController.loginUser = array;
//    [self.navigationController pushViewController:objMenuViewController animated:YES];
//
//    
//    
//}

- (IBAction)forgotButtonAction:(id)sender
{
    PasswordResetViewController *objPasswordResetViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"PasswordResetViewController"];
    [self.navigationController pushViewController:objPasswordResetViewController animated:YES];
}
@end
