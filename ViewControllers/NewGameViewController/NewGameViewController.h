//
//  NewGameViewController.h
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface NewGameViewController : GAITrackedViewController <FBSDKGameRequestDialogDelegate,FBSDKAppInviteDialogDelegate>
@property (strong, nonatomic) IBOutlet UIButton *button1;
- (IBAction)button1Actionperformed:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *button2;
- (IBAction)button2ActionPerformed:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *button3;
- (IBAction)button3Actonperformed:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *button4;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
- (IBAction)button4ActionPerformed:(UIButton *)sender;

@property (strong, nonatomic) FBSDKShareDialog *shareCodeDialog;
@property(nonatomic, assign) int currentIndex;
@end
