//
//  NewGameViewController.m
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "NewGameViewController.h"
#import "MultiplePlayerViewController.h"
#import "QuestionViewController.h"
#import "QuestionWithImageViewController.h"
#import "AppDelegate.h"
#import "ChallangeWithEmailViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "MBProgressHUD.h"
#import "FacebookFriendListViewController.h"

@interface NewGameViewController ()
{
    NSArray *currentPlayer;
    NSString *User_id;
    NSMutableData *receivedData;
    NSArray *array;
    UIViewController *nextView;
    AppDelegate *objAppDelegate;
    NSArray *friends;

}

@end

@implementation NewGameViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     objAppDelegate.locationUseBool = NO;
    objAppDelegate.tagArray =[[NSMutableArray alloc] init];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Neues Spiel";
    //  self.navigationItem.title = @"NEUES SPIEL";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.button2.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    self.button2.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.button2 setTitle: @"ZUFÄLLIGER SPIELER HERAUSFORDERN" forState: UIControlStateNormal];
    
    self.button3.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    self.button3.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.button3 setTitle: @"JEMAND PER E-MAIL HERAUSFORDERN" forState: UIControlStateNormal];
    
    self.button4.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    self.button4.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.button4 setTitle: @"FACEBOOK FREUNDE HERAUSFORDERN" forState: UIControlStateNormal];
   // UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    
//    closeButton.frame = CGRectMake(0, 0, 30, 24);
//    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
//    
//    [closeButton addTarget:self action:@selector(closenew) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
//    
//    self.navigationItem.rightBarButtonItem = btnclose;
    
//    [_button1 setEnabled:NO];
//    _button1.userInteractionEnabled = NO;
//    
//    [_button2 setEnabled:NO];
//    _button2.userInteractionEnabled = NO;
//    [_button3 setEnabled:NO];
//    _button3.userInteractionEnabled = NO;
//    [_button4 setEnabled:NO];
//    _button4.userInteractionEnabled = NO;
     //currentPlayer=self.loginUser ;
//    if (objAppDelegate.loginwithFacebook==YES)
//    {
//         User_id =[objAppDelegate.facebookloginUser  objectForKey:@"id"];
//    }
//    else
//    {
   User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];
//    }
      [self serverCommunication];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"NewGameViewController";
     self.button1.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
     self.button2.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
     self.button3.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
  
    if (nextView != nil) {
        _currentIndex ++;
        if ([[[array objectAtIndex:_currentIndex]valueForKey:@"question_photo"]isEqualToString:@""] )
        {
            QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
            objQuestionViewController.questionsArray =array;
            objQuestionViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionViewController animated:YES];
            
        }
        else
        {
            QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
            objQuestionWithImageViewController.questionsArrayWithImage =array;
            objQuestionWithImageViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
            
            
        }

    }
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
}
-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

    
    

#pragma mark-  connectionMethods
-(void)serverCommunication
{
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *post = [NSString stringWithFormat:@"user_id=%@",User_id];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getRandomQuestions"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }
        
    }
    
#pragma mark-  connectionDelegateMethods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    //    NSError* error;
    //    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
    //                                                         options:kNilOptions
    //                                                           error:&error];
      [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"msg"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        array = json;
        objAppDelegate.QuestionArray = json;
//        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Quiz-Fragen"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        
//               [connectSuccessMessage show];
//        connectSuccessMessage.tag = 1;
        
        
        
    }
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (buttonIndex == 0)
        {
            NSLog(@"Cancel Tapped.");
            [NSTimer scheduledTimerWithTimeInterval:2.0
                                             target:self
                                           selector:@selector(targetMethod)
                                           userInfo:nil
                                            repeats:NO];
            
        }
        
    }
}
-(void)targetMethod
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [_button1 setEnabled:YES];
    _button1.userInteractionEnabled = YES;
    
    [_button2 setEnabled:YES];
    _button2.userInteractionEnabled = YES;
    [_button3 setEnabled:YES];
    _button3.userInteractionEnabled = YES;
    [_button4 setEnabled:YES];
    _button4.userInteractionEnabled = YES;

    
}

-(IBAction)closenew
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-  ButtonActionMethods
- (IBAction)button1Actionperformed:(id)sender
{
    objAppDelegate.challangwithEmail = NO;
    objAppDelegate.locationUseBool = YES;
    [objAppDelegate.tagArray removeAllObjects];
    
    _currentIndex = 0;
    if ([[[array objectAtIndex:0]valueForKey:@"question_photo"]isEqualToString:@""])
    {
        QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
        objQuestionViewController.questionsArray =array;
        objQuestionViewController.currentIndex = 0;
        //objQuestionViewController.loginUser = self.loginUser;
        nextView = objQuestionViewController;
        [self.navigationController pushViewController:objQuestionViewController animated:YES];
       
    }
    else
    {
        QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
        objQuestionWithImageViewController.questionsArrayWithImage =array;
        objQuestionWithImageViewController.currentIndex = 0;
        //objQuestionWithImageViewController.loginUser = self.loginUser;
        nextView = objQuestionWithImageViewController;
    [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
        
      
    }

    
    
    
}
- (IBAction)button3Actonperformed:(id)sender
{
    objAppDelegate.opponantplayerPlay = NO;
    objAppDelegate.challangwithEmail = YES;
    ChallangeWithEmailViewController *objChallangeWithEmailViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"ChallangeWithEmailViewController"];
    objChallangeWithEmailViewController.loginUser =currentPlayer;
    objChallangeWithEmailViewController.questionArray = array;
    objChallangeWithEmailViewController.currentIndex = 0;
    [self.navigationController pushViewController:objChallangeWithEmailViewController animated:YES];
    
    
}
- (IBAction)button4ActionPerformed:(id)sender
{
    
    
    if (![FBSession activeSession].isOpen)
    {
        [self connectWithFacebook];
    }
    else
    {
    
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions/publish_actions"
                                       parameters:nil
                                       HTTPMethod:@"DELETE"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email",@"user_friends"]

                                            allowLoginUI:NO
                                       completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                           FBRequest* friendsRequest = [FBRequest requestForMyFriends];
                                           [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                                                         NSDictionary* result,
                                                                                         NSError *error)
                                            {
                                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                               friends = [result objectForKey:@"data"];
                                                NSLog(@"Found: %lu friends", (unsigned long)friends.count);
                                                for (NSDictionary<FBGraphUser>* friend in friends)
                                                {
                                                    NSLog(@"I have a friend named %@ with id %@", friend.name, friend.objectID);
                                                    
                                                   
                                                    
                                                   
                                                    

                                                }
                                                if (friends) {
                                                    FacebookFriendListViewController *FacebookFriendListViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"FacebookFriendListViewController"];
                                                    FacebookFriendListViewController.FacebbolFriendList = friends;
                                                    
                                                    [self.navigationController pushViewController:FacebookFriendListViewController animated:YES];
                                                }
                                               
                                            }];
                                           
                                           
                                       }];
         

         // ...
     }];
    
}
}
- (void) connectWithFacebook
{
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate openSessionWithAllowLoginUI:YES];
}

- (void)gameRequestDialog:(FBSDKGameRequestDialog *)gameRequestDialog didCompleteWithResults:(NSDictionary *)results
{
    
}
- (void)gameRequestDialog:(FBSDKGameRequestDialog *)gameRequestDialog didFailWithError:(NSError *)error
{
    
}

- (IBAction)button2ActionPerformed:(id)sender
{
    objAppDelegate.opponantplayerPlay = NO;
    MultiplePlayerViewController *objMultiplePlayerViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MultiplePlayerViewController"];
    objMultiplePlayerViewController.loginUser =currentPlayer;
    objMultiplePlayerViewController.questionArray =array;
     objMultiplePlayerViewController.currentIndex = 0;
    [self.navigationController pushViewController:objMultiplePlayerViewController animated:YES];
    
}
@end
