//
//  ChallangeWithEmailViewController.m
//  QuizApp
//
//  Created by Shailendra on 28/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "ChallangeWithEmailViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "QuestionViewController.h"
#import "QuestionWithImageViewController.h"

@interface ChallangeWithEmailViewController ()
{
    BOOL final;

     NSMutableData *receivedData;
    AppDelegate *objAppDelegate;
    UIViewController *nextView;

}

@end

@implementation ChallangeWithEmailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Herausfordern";
   // self.navigationItem.title = @"HERAUSFORDERN";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
      self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    // Do any additional setup after loading the view.
}
#pragma mark-  CustomMethods
-(BOOL)email:(NSString *)mail
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:mail];
    return YES;
    
    
    //    return [self.emailTextField.text containsString:@"redbytes.in"];
}

#pragma mark-  textFieldDelegateMethods
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    BOOL succes;
    succes=[self email:self.emailTextField.text];
    if (succes==NO)
    {
        
        self.emailTextField.placeholder=@"Email-id";
        if (self.emailTextField.text.length>0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte tippen Sie korrekte E-Mail-" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"stornieren", nil];
            [alert setTag:1];

                       [alert show];

            //[self.emailErrorLabel setHidden:FALSE];
                    }
        self.emailTextField.text=@"";
        final=NO;
    }
    else
    {
        //[self.emailErrorLabel setHidden:TRUE];
    }
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
   

   
    [super viewWillAppear:animated];
   
    self.screenName = @"ChallangeWithEmailViewController";
    if (nextView != nil)
    {
        _currentIndex ++;
        if ([[[self.questionArray objectAtIndex:_currentIndex]valueForKey:@"question_photo"]isEqualToString:@""] )
        {
            QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
            objQuestionViewController.questionsArray =self.questionArray;
            objQuestionViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionViewController animated:YES];
            
        }
        else
        {
            QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
            objQuestionWithImageViewController.questionsArrayWithImage =self.questionArray;
            objQuestionWithImageViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
            
            
        }
    }
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
}

-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)checkEmailActionButtonClicked:(id)sender
{
    BOOL success =[self email:self.emailTextField.text];
    if (success)
    {
    [[NSUserDefaults standardUserDefaults ]setValue:self.emailTextField.text forKey:@"opponantemailid"];
    [[NSUserDefaults standardUserDefaults ]synchronize];
    [self serverCommunication];
    }
    else
    {
        
        if ([self.emailTextField.text isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hinweis" message:@"Bitte E-Mail Adresse eingeben" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Abbrechen", nil];
            [alert setTag:1];

            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hinweis" message:@"Bitte geben Sie korrekte E-Mail" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Abbrechen", nil];
            [alert setTag:1];

            [alert show];
            self.emailTextField.text= @"";
            
        }

        //[self.emailErrorLabel setHidden:FALSE];
        
    }
    
   
    
}

-(void)serverCommunication
{
    
    if (self.emailTextField.text.length>0)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *post = [NSString stringWithFormat:@"email=%@",self.emailTextField.text];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=checkEmailExists"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"prüfen Sie den Anschluss " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage setTag: 1];
            [connectFailMessage show];
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte geben Sie alle Felder aus" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert setTag:1];
       
        [alert show];
    }
    
}

#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    //    NSError* error;
    //    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
    //                                                         options:kNilOptions
    //                                                           error:&error];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"error"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"E-Mail nicht vorhanden" message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        objAppDelegate.opponantUser = json;
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:[[json objectAtIndex:0] valueForKey:@"uid"]];
        objAppDelegate.opponantPlayerId = myNumber;
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Benutzer registrieren " message:@"Email gibt es in datenbank" delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
       
        
    }
    
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (alertView.tag == 1)
    {
      [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
    else
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Cancel Tapped.");
            [NSTimer scheduledTimerWithTimeInterval:2.0
                                             target:self
                                           selector:@selector(targetMethod)
                                           userInfo:nil
                                            repeats:NO];
            
        }
        
    }
    
}
-(void)targetMethod
{
     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    _currentIndex = 0;
    if ([[[self.questionArray objectAtIndex:0]valueForKey:@"question_photo"]isEqualToString:@""])
    {
        QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
        objQuestionViewController.questionsArray =self.questionArray;
        objQuestionViewController.currentIndex = 0;
        nextView = objQuestionViewController;
        [self.navigationController pushViewController:objQuestionViewController animated:YES];
        
    }
    else
    {
        QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
        objQuestionWithImageViewController.questionsArrayWithImage =self.questionArray;
        objQuestionWithImageViewController.currentIndex = 0;
        nextView = objQuestionWithImageViewController;
        [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
        
        
    }
    

    
}

@end
