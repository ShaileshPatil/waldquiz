//
//  ChallangeWithEmailViewController.h
//  QuizApp
//
//  Created by Shailendra on 28/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ChallangeWithEmailViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)checkEmailActionButtonClicked:(id)sender;
@property(nonatomic, assign) int currentIndex;

@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (strong, nonatomic) IBOutlet NSArray *questionArray;
@end
