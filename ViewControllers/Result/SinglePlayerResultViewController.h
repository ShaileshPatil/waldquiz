//
//  SinglePlayerResultViewController.h
//  QuizApp
//
//  Created by Shailendra on 23/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "GAITrackedViewController.h"

@interface SinglePlayerResultViewController : GAITrackedViewController

@property (strong, nonatomic) IBOutlet UIView *customView;
@property (strong, nonatomic) IBOutlet AsyncImageView *customeImageView;
@property (strong, nonatomic) IBOutlet UILabel *customNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *customPointLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTextLabel;

@property (strong, nonatomic) IBOutlet UIButton *HomeButtonActionPerformed;
@property (strong, nonatomic) IBOutlet NSArray *FinalResponseArray;
- (IBAction)HomeButtonClicked:(id)sender;

//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@end
