//
//  MultiplePlayerResultViewController.m
//  QuizApp
//
//  Created by Shailendra on 23/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "MultiplePlayerResultViewController.h"
#import "AppDelegate.h"
#import "HomeViewController.h"

@interface MultiplePlayerResultViewController ()
{
     AppDelegate *objAppDelegate;
    NSArray *loginUser,*oponantUser;
}

@end

@implementation MultiplePlayerResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    // self.view.backgroundColor = [UIColor colorWithRed:234/255.0 green:224/255.0 blue:162/255.0 alpha:1];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Resultat";
    //self.navigationItem.title = @"RESULTAT";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"HOME" style:UIBarButtonItemStylePlain target:self action:@selector(homeView)];
    self.navigationItem.rightBarButtonItem = anotherButton;

}
-(void)homeView
{
    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
    // objSinglePlayerResultViewController.loginUser = self.loginUser;
    objAppDelegate.mark = 0;
    objHomeViewController.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:objHomeViewController animated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated
{
      [self.multilineTextView setHidden:NO];
    [super viewWillAppear:animated];
    self.screenName = @"MultiplePlayerResultViewController";
    loginUser = objAppDelegate.loginUser;
    oponantUser = objAppDelegate.opponantUser;
    
    
    self.navigationItem.hidesBackButton = YES;
   self.HomeButtonClickActionPerformed.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    // self.customeImageView.image =
    UIColor *borderColor = [UIColor colorWithRed:182 green:10 blue:96 alpha:1.0];
    [self.firstCustomView.layer setBorderColor:borderColor.CGColor];
    [self.firstCustomView.layer setBorderWidth:1.0];
    UIColor *borderColor1 = [UIColor colorWithRed:182 green:10 blue:96 alpha:1.0];
    [self.secondCustomView.layer setBorderColor:borderColor1.CGColor];
    [self.secondCustomView.layer setBorderWidth:1.0];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Success"] isEqualToString:@"YES"])
    {
        [self.multilineTextView setHidden:YES];
        self.secondCustomImageView.imageURL = [NSURL URLWithString:(NSString *)[self.FinalResponseArray valueForKey:@"creator_image"]];
        NSString *varyingString1 = [self.FinalResponseArray valueForKey:@"creator_fname"];
        NSString *varyingString2 = [self.FinalResponseArray valueForKey:@"creator_lname"];
        NSString *CreatorName = [NSString stringWithFormat: @"%@ %@", varyingString1, varyingString2];
        self.secondCustomNameLabel.text = CreatorName;
         self.secondCustomPointLabel.text  =@"";
        self.secondCustomPointLabel.text = [self.secondCustomPointLabel.text stringByAppendingString:[NSString stringWithFormat: @"%@ PUNKTE",[self.FinalResponseArray valueForKey:@"creator_points"]]];
        
        self.firstCustomImageView.imageURL =[NSURL URLWithString:(NSString *)[self.FinalResponseArray  valueForKey:@"opponent_image"]];
        NSString *varyingString3 = [self.FinalResponseArray valueForKey:@"opponent_fname"];
        NSString *varyingString4 = [self.FinalResponseArray valueForKey:@"opponent_lname"];
        NSString *OpponantName = [NSString stringWithFormat: @"%@ %@", varyingString3, varyingString4];
        self.firstCustomNameLabel.text=OpponantName;
        
        self.firstCustomPointsLabel.text  =@"";
        self.firstCustomPointsLabel.text = [self.firstCustomPointsLabel.text stringByAppendingString:[NSString stringWithFormat: @"%@ PUNKTE",[self.FinalResponseArray valueForKey:@"opponent_points"]]];
        NSInteger total = objAppDelegate.totalMark;
        objAppDelegate.totalMark =total+(long)objAppDelegate.mark;
         self.bannerImageView.image =[UIImage imageNamed:@"text.png"];
        [self.HomeButtonClickActionPerformed setTitle:@"ZURUCK ZUM START" forState:normal];
        self.nameLabel.backgroundColor = [ UIColor clearColor];
        self.pointLabel.backgroundColor = [UIColor clearColor];
        self.subTextLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.text = @"Herzlichen Gluckwunsch\ndu bekommst";
        self.pointLabel.text  =@"";
        self.pointLabel.text = [self.pointLabel.text stringByAppendingString:[NSString stringWithFormat: @"+%@  Punkte", [self.FinalResponseArray valueForKey:@"your_points"]]];
        self.subTextLabel.text= @"fur deinen Sieg!!.";
        
    }
    else
    {
        //

        self.firstCustomImageView.imageURL = [NSURL URLWithString:(NSString *)[self.FinalResponseArray valueForKey:@"creator_image"]];
        NSString *varyingString1 = [self.FinalResponseArray valueForKey:@"creator_fname"];
        NSString *varyingString2 = [self.FinalResponseArray valueForKey:@"creator_lname"];
        NSString *CreatorName = [NSString stringWithFormat: @"%@ %@", varyingString1, varyingString2];
        self.firstCustomNameLabel.text = CreatorName;
        

       self.firstCustomPointsLabel.text  =@"";
       self.firstCustomPointsLabel.text = [self.firstCustomPointsLabel.text stringByAppendingString:[NSString stringWithFormat: @"%ld PUNKTE", (long)objAppDelegate.mark]];
//            NSInteger total = objAppDelegate.totalMark;
//            
//            objAppDelegate.totalMark =total+(long)objAppDelegate.mark;
            
            NSString *string = [self.FinalResponseArray valueForKey:@"creator_total_points"];
            objAppDelegate.totalMark = [string intValue];
     
    
    if (objAppDelegate.responseTypeInJson == YES)
    {
        
        self.secondCustomImageView.imageURL =[NSURL URLWithString:(NSString *)[self.FinalResponseArray  valueForKey:@"opponent_image"]];
        NSString *varyingString3 = [self.FinalResponseArray valueForKey:@"opponent_fname"];
        NSString *varyingString4 = [self.FinalResponseArray valueForKey:@"opponent_lname"];
        NSString *OpponantName = [NSString stringWithFormat: @"%@ %@", varyingString3, varyingString4];
        self.secondCustomNameLabel.text=OpponantName;

        
    }
    else
    {
        if (objAppDelegate.loginwithFacebook == YES)
        {
            NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
            self.secondCustomImageView.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
            self.secondCustomNameLabel.text= [objAppDelegate.opponantUser valueForKey:@"name"];
            
            objAppDelegate.loginwithFacebook = NO;
           

            
        }
        else
        {
          self.secondCustomImageView.imageURL =[NSURL URLWithString:(NSString *)[self.FinalResponseArray  valueForKey:@"opponent_image"]];
        NSString *varyingString3 = [self.FinalResponseArray valueForKey:@"opponent_fname"];
        NSString *varyingString4 = [self.FinalResponseArray valueForKey:@"opponent_lname"];
        NSString *OpponantName = [NSString stringWithFormat: @"%@ %@", varyingString3, varyingString4];
        self.secondCustomNameLabel.text=OpponantName;
        }

        
    }
     
         self.secondCustomPointLabel.text = @"OFFEN";
       self.bannerImageView.image =[UIImage imageNamed:@"new_text@1x.png"];
        [self.nameLabel setHidden:YES];
        [self.pointLabel setHidden:YES];
        [self.subTextLabel setHidden:YES];
        self.multilineTextView.backgroundColor = [UIColor clearColor];
        self.multilineTextView.text = @"Andress Frei muss diese Fragen \n noch beantworten. Du bekommst\n  die Punkte automatisch falls du\n  gewinnst.";

     }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)HomeButtonClicked:(id)sender
{
    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
    // objSinglePlayerResultViewController.loginUser = self.loginUser;
    objAppDelegate.mark = 0;
    objHomeViewController.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:objHomeViewController animated:YES];
}
@end
