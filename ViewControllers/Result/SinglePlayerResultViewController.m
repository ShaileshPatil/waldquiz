//
//  SinglePlayerResultViewController.m
//  QuizApp
//
//  Created by Shailendra on 23/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "SinglePlayerResultViewController.h"
#import "AppDelegate.h"
#import "HomeViewController.h"

@interface SinglePlayerResultViewController ()
{
     AppDelegate *objAppDelegate;
    NSArray *loginUser;
}

@end

@implementation SinglePlayerResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Resultat";
    //self.navigationItem.title = @"RESULTAT";

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"HOME" style:UIBarButtonItemStylePlain target:self action:@selector(homeView)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"SinglePlayerResultViewController";
    self.navigationItem.hidesBackButton = YES;
    loginUser = objAppDelegate.loginUser;
   // self.customeImageView.image =
    UIColor *borderColor = [UIColor colorWithRed:182 green:10 blue:96 alpha:1.0];
    [self.customeImageView.layer setBorderColor:borderColor.CGColor];
    [self.customeImageView.layer setBorderWidth:1.0];
    
    self.HomeButtonActionPerformed.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];

    if (objAppDelegate.loginwithFacebook == YES)
    {
        
        NSString *defaultUrl =  @"https://www.waldquiz.ch/uploads/tx_game/rank/vogel_0-100.png";
        self.customeImageView.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
//        self.customeImageView.imageURL =[NSURL URLWithString:(NSString *)[objAppDelegate.facebookloginUser  objectForKey:@"image"]];
        
        self.customNameLabel.text =[objAppDelegate.facebookloginUser  objectForKey:@"name"];
         self.customPointLabel.text  =@"";
        self.customPointLabel.text =[self.customPointLabel.text stringByAppendingString:[NSString stringWithFormat: @"%ld PUNKTE", (long)objAppDelegate.mark]];

    }
    else
    {
    if ([[self.FinalResponseArray valueForKey:@"image"]isEqualToString:@""])
    {
        self.customeImageView.image=[UIImage imageNamed:@"chat.png"] ;
    }
    else
    {
         self.customeImageView.imageURL =[NSURL URLWithString:(NSString *)[self.FinalResponseArray valueForKey:@"image"]];
//        self.customeImageView.imageURL= [UIImage imageNamed:([[loginUser objectAtIndex:0]valueForKey:@"image"] )];
    }
    
    NSString *varyingString1 = [self.FinalResponseArray valueForKey:@"first_name"];
    NSString *varyingString2 = [self.FinalResponseArray valueForKey:@"last_name"];
    NSString *CreatorName = [NSString stringWithFormat: @"%@ %@", varyingString1, varyingString2];
    self.customNameLabel.text = CreatorName;

    
//    self.customNameLabel.text=[[loginUser objectAtIndex:0]valueForKey:@"first_name"];
    //NSString *inStr = [NSString stringWithFormat: @"%ld", (long)objAppDelegate.mark];
    self.customPointLabel.text  =@"";
    self.customPointLabel.text = [self.customPointLabel.text stringByAppendingString:[NSString stringWithFormat: @"%ld PUNKTE", (long)objAppDelegate.mark]];
        NSInteger total = objAppDelegate.totalMark;
        objAppDelegate.totalMark =total+(long)objAppDelegate.mark;
    }
    self.nameLabel.backgroundColor = [ UIColor clearColor];
    self.pointLabel.backgroundColor = [UIColor clearColor];
    self.subTextLabel.backgroundColor = [UIColor clearColor];
    
    self.nameLabel.text = @"Du bekommst";
     self.pointLabel.text  =@"";
    self.pointLabel.text = [self.pointLabel.text stringByAppendingString:[NSString stringWithFormat: @"+%ld  Punkte", (long)objAppDelegate.mark]];
    self.subTextLabel.text= @"auf dein Konto!";
    self.nameLabel.textColor = [UIColor blackColor];
    self.pointLabel.textColor = [UIColor blackColor];
    self.subTextLabel.textColor = [UIColor blackColor];
    self.bannerImageView.image =[UIImage imageNamed:@"text_2.png"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)homeView
{
    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
    // objSinglePlayerResultViewController.loginUser = self.loginUser;
    objAppDelegate.mark = 0;
    objHomeViewController.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:objHomeViewController animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)HomeButtonClicked:(id)sender
{
    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
    // objSinglePlayerResultViewController.loginUser = self.loginUser;
    objAppDelegate.mark = 0;
    objHomeViewController.navigationItem.hidesBackButton = YES;
    [self.navigationController pushViewController:objHomeViewController animated:YES];
    
}
@end
