//
//  MultiplePlayerResultViewController.h
//  QuizApp
//
//  Created by Shailendra on 23/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "AsyncImageView.h"

@interface MultiplePlayerResultViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet UIView *firstCustomView;
@property (strong, nonatomic) IBOutlet AsyncImageView *firstCustomImageView;
@property (strong, nonatomic) IBOutlet UILabel *firstCustomNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstCustomPointsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTextLabel;
@property (strong, nonatomic) IBOutlet UIView *secondCustomView;
@property (strong, nonatomic) IBOutlet AsyncImageView *secondCustomImageView;
@property (strong, nonatomic) IBOutlet UILabel *secondCustomNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondCustomPointLabel;
@property (strong, nonatomic) IBOutlet UIButton *HomeButtonClickActionPerformed;
@property (strong, nonatomic) IBOutlet NSArray *FinalResponseArray;

- (IBAction)HomeButtonClicked:(id)sender;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (weak, nonatomic) IBOutlet UITextView *multilineTextView;


@end
