//
//  MenuViewController.m
//  QuizApp
//
//  Created by Shailendra on 30/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "MenuViewController.h"
#import "HomeViewController.h"
#import "RankViewController.h"
#import "SettingViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSDataAdditions.h"
#import "InstructionViewController.h"
#import "PrivatePolicyViewController.h"
#import "TermsViewController.h"
#import "AboutUsViewController.h"
#import "LoginViewController.h"
#import "ViewController.h"
//#import "Base64.h"



@interface MenuViewController ()
{
    NSData *imageData;
    UIImage *image;
    UIImage *selectedPhoto;
    NSMutableArray *List;
     NSArray *user;
    AppDelegate *objAppDelegate;
       NSMutableData *receivedData;
    NSString *User_id;
    NSInteger buttonIndexcount;
   


}

@end

@implementation MenuViewController
  @synthesize loginUser;
@synthesize imageString;
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    
    [super viewDidLoad];
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     [[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.circularProfileImageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    self.circularProfileImageView.layer.cornerRadius=25;
    self.circularProfileImageView.layer.borderWidth=1.0;
    self.circularProfileImageView.layer.masksToBounds = YES;
    //self.circularProfileImageView.layer.borderColor=[[UIColor redColor] CGColor];
    
    
    self.circularSideImageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    self.circularSideImageView.layer.cornerRadius=25;
    self.circularSideImageView.layer.borderWidth=1.0;
    self.circularSideImageView.layer.masksToBounds = YES;
   // self.circularSideImageView.layer.borderColor=[[UIColor redColor] CGColor];
    self.menuTableView.backgroundColor = [UIColor colorWithRed:58/255.0 green:69/255.0 blue:78/255.0 alpha:1];
    
    // List=[[NSMutableArray alloc]initWithObjects:@"HOME",@"RANGLISTE",@"SPIELANLEITUNG",@"AGB",@"IMPRESSUM",@"DATENSCHUTZ",@"LOGOUT", nil];
    List=[[NSMutableArray alloc]initWithObjects:@"Home",@"Rangliste",@"Spielanleitung",@"AGB",@"Impressum",@"Datenschutz",@"Logout", nil];
  
    
        //UIImage *btnImage = [UIImage imageNamed:@"image.png"];
    //[btnTwo setImage:btnImage forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"MenuViewController";
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    NSArray *loginUserValue = [[NSUserDefaults standardUserDefaults]
                          valueForKey:@"loginUser"];
    
    if (loginUserValue != nil)
    {
        objAppDelegate.loginUser = loginUserValue;
        
    }

    loginUser=objAppDelegate.loginUser ;
    
   // self.circularProfileImageView.imageURL =[NSURL URLWithString:(NSString *)[[loginUser objectAtIndex:0]valueForKey:@"image"]];
    self.circularProfileImageView.imageURL = [NSURL URLWithString:(NSString *) objAppDelegate.profileImageForUser];
    
    User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];

    NSString *inStr = [NSString stringWithFormat: @"%ld", (long)objAppDelegate.totalMark];
       self.nameLabel.text=[[loginUser objectAtIndex:0]valueForKey:@"first_name"];
    //self.pointLabel.text =[[loginUser objectAtIndex:0]valueForKey:@"points"];
    self.pointLabel.text =inStr;
    self.statusLabel.text =[[loginUser objectAtIndex:0]valueForKey:@"rank_name"];
   self.circularSideImageView.imageURL =[NSURL URLWithString:(NSString *)[[loginUser objectAtIndex:0]valueForKey:@"rank_icon"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-  cameraButtonAction
- (IBAction)CameraButtionClicked:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Bildauswahl" delegate:self cancelButtonTitle:@"Abbrechen" destructiveButtonTitle:nil otherButtonTitles:
                            @"Foto auswählen ",
                            @"Foto aufnehmen",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (popup.tag)
    {
        case 1:
        {
            switch (buttonIndex)
            {
                    
                case 0:
                    buttonIndexcount = buttonIndex;
                    [self importPhotoButtonClicked];
                    break;
                case 1:
                    buttonIndexcount = buttonIndex;
                    [self takePhotoButtonClicked];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)takePhotoButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Foto"
                                                              message:@"Kamera ist nicht in Ihrem Gerät"
                                                             delegate:nil
                                                    cancelButtonTitle:@"stornieren"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
        
        // Resize image
       
    }
    
}
// use photo from PhotoLibrary
- (void)importPhotoButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //NSString *mediaType = info[UIImagePickerControllerMediaType];
    UIImage *image1 = info[UIImagePickerControllerOriginalImage];
    //self.selectImageView.image = image;
    
   
    imageData = UIImageJPEGRepresentation(image1, 0.05f);
    //imageData = [theImage getData];
   
    //self.circularProfileImageView.image = [UIImage imageWithData:imageData];
    //NSData *data = UIImagePNGRepresentation(selectedPhoto) ;
    
    //imageString = [imageData base64EncodedStringWithOptions:0];
    NSLog(@"%@", imageString);
    imageString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self serverCommunication];
    
}

#pragma mark UIImagePickerControllerDelegate
//- (void) imagePickerController:(UIImagePickerController *)picker
//         didFinishPickingImage:(UIImage *)image1
//                   editingInfo:(NSDictionary *)editingInfo
//{
//    
//    //self.uiimageView.image=image1;
//    // Resize image
//    if (image1 != nil)
//    {
//        imageData = UIImagePNGRepresentation(image1);
//        selectedPhoto = [UIImage imageWithData:imageData];
//        //self.circularProfileImageView.image = selectedPhoto;
//            imageString = [imageData base64EncodedStringWithOptions:0];
//        NSLog(@"%@", imageString);
////        NSData *data = [[NSData alloc]initWithBase64EncodedString:imageString options:0];
//        //self.circularSideImageView.image = [UIImage imageWithData:data];
//          self.circularProfileImageView.image = image1;
//
//        [self serverCommunication];
//
//    }
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    selectedPhoto = nil;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)serverCommunication
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString *strImageData = [imageString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSString *post = [NSString stringWithFormat:@"file_name=%@&user_id=%@",strImageData,User_id];
    

    //post = [post stringByAddingPercentEscapesUsingEncoding:
            //NSUTF8StringEncoding];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding ];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=setUserPicture"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSLog(@"request=%@",request);
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

if (connection)
{
    NSLog(@"Connection Successful");
}
else
{
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
}

}
#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        NSString* responseError = [json valueForKey:@"error"];
        
        NSLog(@"error: %@", responseError);
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Bild Hochladen " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }
    else if ([json isKindOfClass:[NSArray class]] )
    {
        objAppDelegate.profileImageForUser =[[json objectAtIndex:0]valueForKey:@"image"];
        self.circularProfileImageView.imageURL = [NSURL URLWithString:(NSString *)[[json objectAtIndex:0]valueForKey:@"image"]];
        NSArray *array = json;
        NSLog(@"%@",array);
        
        if (buttonIndexcount == 0)
        {
            UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Bild Hochladen " message:@"Benutzer erfolgreich Hochgeladen Bild"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectSuccessMessage setTag:1];
            [connectSuccessMessage show];
        }
        else
        {
            UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Bild Hochladen " message:@"Foto erfolgreich hochgeladen"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectSuccessMessage setTag:1];
            [connectSuccessMessage show];

            
        }
       
        
        
    }
    
    
}


- (IBAction)SettingButtonClicked:(id)sender
{
    SettingViewController *objSettingViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
     {
         
         [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
         
         
         [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objSettingViewController]  animated:NO];
     }];

   
//    [self.navigationController pushViewController:objSettingViewController animated:YES];
    
}
- (IBAction)ShereButtonClicked:(id)sender
{
    
}
#pragma mark-  TableViewDelegateAndDatesourseMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return List.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor colorWithRed:58/255.0 green:69/255.0 blue:78/255.0 alpha:1];
    cell.textLabel.text =[List objectAtIndex:indexPath.row];
    cell.textLabel.textColor= [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
        //objHomeViewController.loginUser =user;
        //objAppDelegate.hidebackButton = YES;
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objHomeViewController]  animated:NO];
         }];
        
    }
    if (indexPath.row == 1)
    {
        objAppDelegate.menuvalue = YES;
        RankViewController *objRankViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"RankViewController"];
        //objRankViewController.CheckingValue = @"Cheack";
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objRankViewController]  animated:NO];
         }];

        
    }
    if (indexPath.row == 2)
    {
        InstructionViewController *objInstructionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"InstructionViewController"];
        //objRankViewController.CheckingValue = @"Cheack";
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objInstructionViewController]  animated:NO];
         }];
        
        
    }
    if (indexPath.row == 3)
    {
        TermsViewController *objTermsViewControlle= [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TermsViewController"];
        //objRankViewController.CheckingValue = @"Cheack";
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objTermsViewControlle]  animated:NO];
         }];
        
        
    }
    if (indexPath.row == 4)
    {
        AboutUsViewController *objAboutUsViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
        //objRankViewController.CheckingValue = @"Cheack";
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objAboutUsViewController]  animated:NO];
         }];
        
        
    }

    if (indexPath.row == 5)
    {
        PrivatePolicyViewController *objPrivatePolicyViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"PrivatePolicyViewController"];
        //objRankViewController.CheckingValue = @"Cheack";
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objPrivatePolicyViewController]  animated:NO];
         }];
        
        
    }
    if (indexPath.row == 6)
    {
        objAppDelegate.hidebackButtonforlogin = NO;
        NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
        [removeUD removeObjectForKey:@"loginUser"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        NSUserDefaults * removeUD1 = [NSUserDefaults standardUserDefaults];
        [removeUD1 removeObjectForKey:@"userName"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        NSUserDefaults * removeUD2 = [NSUserDefaults standardUserDefaults];
        [removeUD2 removeObjectForKey:@"passWord"];
        [[NSUserDefaults standardUserDefaults]synchronize ];
        
        [FBSession.activeSession closeAndClearTokenInformation];
//        LoginViewController *objLoginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
//         {
//             
//             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
//             
//             
//             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objLoginViewController]  animated:NO];
//         }];
//        
        
        
        ViewController *objViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
         {
             
             [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
             
             
             [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objViewController]  animated:NO];
         }];
        

        
//        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"ALERT " message:@"Implimentation still working"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        
//        [connectSuccessMessage show];
    }





}

@end
