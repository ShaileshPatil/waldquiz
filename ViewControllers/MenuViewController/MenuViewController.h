//
//  MenuViewController.h
//  QuizApp
//
//  Created by Shailendra on 30/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"
#import "AsyncImageView.h"



@interface MenuViewController : GAITrackedViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet AsyncImageView *circularProfileImageView;
@property (weak, nonatomic) IBOutlet AsyncImageView *circularSideImageView;
- (IBAction)CameraButtionClicked:(id)sender;
- (IBAction)SettingButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
- (IBAction)ShereButtonClicked:(id)sender;
@property (strong, nonatomic) NSString* imageString;
@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
