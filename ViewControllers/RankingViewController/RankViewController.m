//
//  RankViewController.m
//  QuizApp
//
//  Created by Shailendra on 15/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "RankViewController.h"
#import "CustomTableViewCell.h"
#import "MBProgressHUD.h"
#import "AsyncImageView.h"
#import "AppDelegate.h"

@interface RankViewController ()
{
    NSMutableArray *nameArray;
    BOOL final;
    NSMutableData *receivedData;
     NSMutableArray  *idofUser;
     AppDelegate *objAppDelegate;
}

@end

@implementation RankViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
      objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [super viewDidLoad];
    [self serverCommunication];
    
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    if (self.CheckingValue > 0)
    {
         self.navigationItem.title = @"Facebook-Freunde";
        //self.navigationItem.title = @"FACEBOOK-FREUNDE";
    }
    else
    {
         self.navigationItem.title = @"Rangliste";
        //self.navigationItem.title = @"RANGLISTE";

    }
   
   [self.ranktableView setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 24);
    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    
    //    [closeButton addTarget:self action:@selector(closenew:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self.viewDeckController action:@selector(toggleRightView)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;

    nameArray =[[NSMutableArray alloc]init];
    idofUser= [[NSMutableArray alloc]init];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    //     [self serverCommunication];
    [super viewWillAppear:animated];
    self.screenName = @"RankViewController";
    
    if (objAppDelegate.menuvalue == YES)
    {
        self.navigationItem.hidesBackButton = YES;
        objAppDelegate.menuvalue = NO;
        
    }
    else
    {
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
    }
}
-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];
    
}



-(IBAction)closenew:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-TableView Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [idofUser count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    //3
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"CustomTableViewCell" owner:self options:nil];
        cell=(CustomTableViewCell *) [nibbb objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
   
    if (self.CheckingValue > 0)
    {
        cell.customNameLabel.text =[[idofUser objectAtIndex:indexPath.section] valueForKey:@"first_name"];
        cell.cutomImageview.backgroundColor =[UIColor greenColor];
        cell.cutomImageview.image= [UIImage imageNamed:@"chat.png"];
        cell.countLabel.text =@"SPIELEN";
    }
    else
    {
//        NSInteger index=indexPath.row;
//        NSDictionary *myDict=[idofUser objectAtIndex:indexPath.row];
        cell.customNameLabel.text =[[idofUser objectAtIndex:indexPath.section] valueForKey:@"first_name"];
        cell.cutomImageview.backgroundColor =[UIColor greenColor];
        //cell.cutomImageview.image= [UIImage imageNamed:@"chat.png"];
         cell.cutomImageview.imageURL =[NSURL URLWithString:(NSString *)[[idofUser objectAtIndex:indexPath.section] valueForKey:@"image"]];
        cell.countLabel.text =[[idofUser objectAtIndex:indexPath.section] valueForKey:@"points"];
        
    }
    
    
    //cell.cityNameLbl.text = [NSString stringWithFormat:@"Day %d",i];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

#pragma mark-  connectionMethods
-(void)serverCommunication
{
//NSString *postLength;
//NSData *postData;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getRankList"]];



NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

if (connection)
{
    NSLog(@"Connection Successful");
}
else
{
    UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [connectFailMessage show];
}



}
#pragma mark-Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:kNilOptions
                                                           error:&error];
    
    NSString* responseError;
    if ([json count] == 0)
    {
        NSLog(@"empty");
        responseError = @"User Not Available";
    }
    else
    {
        [idofUser removeAllObjects];
        for (NSDictionary *dict in json )
        {
            [idofUser addObject:dict];
        }
        
    }
    
    
    
    NSLog(@"error: %@", responseError);
    
    if ([responseError length] > 0 )
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
        
    }
    else
    {
        
//                    UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@" " message:@"This all Users Name And Rank Number"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [connectSuccessMessage setTag:1];
//            [connectSuccessMessage show];
        [self.ranktableView reloadData];

            
      
        
        
        
        
    }
    
    
    
}
#pragma mark-AlertView Delegate Methods6
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag==1)
//    {
//        if (buttonIndex == 0)
//        {
//            NSLog(@"Cancel Tapped.");
//            
//        }
//        
//    }
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
