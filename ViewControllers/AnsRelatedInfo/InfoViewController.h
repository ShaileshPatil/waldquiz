//
//  InfoViewController.h
//  QuizApp
//
//  Created by Shailendra on 05/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface InfoViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (strong, nonatomic) IBOutlet UIView *mainView;


@property(nonatomic, assign) int currentIndex;

- (IBAction)nextButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet NSArray *questionsArray;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (strong, nonatomic) IBOutlet UIButton *nextButtonClicked;

@end
