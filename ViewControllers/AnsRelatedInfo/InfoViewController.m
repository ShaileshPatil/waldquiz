//
//  InfoViewController.m
//  QuizApp
//
//  Created by Shailendra on 05/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "InfoViewController.h"
#import "QuestionViewController.h"
#import "QuestionWithImageViewController.h"
#import "SinglePlayerResultViewController.h"
#import "MultiplePlayerResultViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface InfoViewController ()
{
    AppDelegate *objAppDelegate;
    //que1=93&sol1=3&ans1=3&que2=285&sol2=2&ans2=2&que3=55&sol3=1&ans3=1
    int que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3,i;
     NSMutableData *receivedData;
    NSNumber *opponanatId ;
    NSString *User_id,*opponantemailid;
    NSArray *FinalResponse;
    
}


@end

@implementation InfoViewController
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    i = 0;
    [super viewDidLoad];
     objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationItem.title = @"Frage 2/3";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];
    
   
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"InfoViewController";
    
    [self.mainView setBackgroundColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];

    [self.infoTextView setBackgroundColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];

     self.nextButtonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    [self.infoTextView setTextColor: [UIColor whiteColor]];
    self.infoTextView.text = [[self.questionsArray objectAtIndex:self.currentIndex]valueForKey:@"info"];
    NSString *string = [[self.questionsArray objectAtIndex:i]valueForKey:@"uid"];
       que1 =[string intValue];
    NSString *ansstring = [[self.questionsArray objectAtIndex:i]valueForKey:@"solution"];
    NSArray *ansarray = [ansstring componentsSeparatedByString:@"n"] ;
   
    NSString *solution = [ansarray objectAtIndex:1];
     sol1 =[solution intValue];
       NSString *string1 = [[self.questionsArray objectAtIndex:i+1]valueForKey:@"uid"];
   que2 =[string1 intValue];
     NSString *ansstring1 = [[self.questionsArray objectAtIndex:i+1]valueForKey:@"solution"];
    NSArray *ansarray1 = [ansstring1 componentsSeparatedByString:@"n"] ;
    
    NSString *solution1 = [ansarray1 objectAtIndex:1];
    sol2 =[solution1 intValue];

    NSString *string2 = [[self.questionsArray objectAtIndex:i+2]valueForKey:@"uid"];
      que3  =[string2 intValue];
     NSString *ansstring2 = [[self.questionsArray objectAtIndex:i+2]valueForKey:@"solution"];
    NSArray *ansarray2 = [ansstring2 componentsSeparatedByString:@"n"] ;
    
    NSString *solution2 = [ansarray2 objectAtIndex:1];
    sol3 =[solution2 intValue];
    if (self.currentIndex == 2)
    {
        NSString *answer1 = [objAppDelegate.tagArray objectAtIndex:0];
        ans1 =[answer1 intValue];
        NSString *answer2 = [objAppDelegate.tagArray objectAtIndex:1];
        ans2 =[answer2 intValue];
        NSString *answer3 = [objAppDelegate.tagArray objectAtIndex:2];
        ans3 =[answer3 intValue];

    }
   
        User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];
    
    
 opponanatId=  objAppDelegate.opponantPlayerId;

    NSString *titleString =[NSString stringWithFormat:@"Frage %d/3",self.currentIndex+1];
    self.navigationItem.title =titleString;
     opponantemailid = [[NSUserDefaults standardUserDefaults]valueForKey:@"opponantemailid"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextButtonClicked:(id)sender
{
   
    
    if(_currentIndex < [_questionsArray count]-1)
    {
        NSArray *viewControllers = [self.navigationController viewControllers];
        [self.navigationController popToViewController:[viewControllers objectAtIndex:viewControllers.count-3] animated:NO];
    }
    else
    {
        if (objAppDelegate .locationUseBool == YES)
        {
           [self serverCommunication];
            

        }
        else
        {
           [self serverCommunication];
            
            
            
        }
        

    }
    
    

   
}

-(void)serverCommunication
{
    //NSString *postLength;
    //NSData *postData;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *temppost;
    if (objAppDelegate.loginwithFacebook == YES)
    {
        
         temppost = [NSString stringWithFormat:@"user_id=%@&creator_id=%@&fb_friend_id=%@&game_type=4&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,User_id,opponanatId,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
    }
   else if (objAppDelegate.challangwithEmail == YES)
    {
        if (objAppDelegate.opponantUser != nil)
        {
            temppost = [NSString stringWithFormat:@"user_id=%@&creator_id=%@&opponent_id=%@&game_type=3&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,User_id,opponanatId,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
            
        }
        else
        {
       temppost = [NSString stringWithFormat:@"user_id=%@&creator_id=%@&opponent_email=%@&game_type=3&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,User_id,opponantemailid,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
        }

    }
    else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Success"] isEqualToString:@"YES"])
    {
       temppost = [NSString stringWithFormat:@"user_id=%@&opponent_id=%@&game_type=3&challenge_id=%@&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,User_id,objAppDelegate.challangeId,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
    
    }
    else if (objAppDelegate .locationUseBool == YES)
    {
        temppost = [NSString stringWithFormat:@"user_id=%@&game_type=1&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
        
        
    }

    else
    {
        temppost = [NSString stringWithFormat:@"user_id=%@&creator_id=%@&opponent_id=%@&game_type=2&que1=%d&sol1=%d&ans1=%d&que2=%d&sol2=%d&ans2=%d&que3=%d&sol3=%d&ans3=%d",User_id,User_id,opponanatId,que1,sol1,ans1,que2,sol2,ans2,que3,sol3,ans3];
        
    }

    
    NSData *postData =
     [temppost dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=saveGameResult"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        NSLog(@"Connection Successful");
    }
    else
    {
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
    }
    
    
    
}
#pragma mark-Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"response data - %@",[[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                              options:kNilOptions
                                                error:&error];
    
    
    
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
        
        
        FinalResponse = json;
        NSLog(@"NSArray%@",FinalResponse);
        
        if (objAppDelegate .locationUseBool == YES)
        {
            SinglePlayerResultViewController *objSinglePlayerResultViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SinglePlayerResultViewController"];
            // objSinglePlayerResultViewController.loginUser = self.loginUser;
            objSinglePlayerResultViewController.FinalResponseArray = FinalResponse;
            [self.navigationController pushViewController:objSinglePlayerResultViewController animated:YES];
        }
        else
        {
            
            
            MultiplePlayerResultViewController *objMultiplePlayerResultViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MultiplePlayerResultViewController"];
            objMultiplePlayerResultViewController.FinalResponseArray =FinalResponse;
            //objMultiplePlayerResultViewController.loginUser = self.loginUser;
            [self.navigationController pushViewController:objMultiplePlayerResultViewController animated:YES];
        }
//        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"User- Spiel erfolgreich complited"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        connectSuccessMessage.tag = 1 ;
//        [connectSuccessMessage show];

        
        
    }
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSString* responseError = [json valueForKey:@"error"];
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];

        
    }
    
    
}
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    if (alertView.tag==1)
//    {
//    
//    if (buttonIndex == 0)
//    {
//        NSLog(@"Cancel Tapped.");
//        if (objAppDelegate .locationUseBool == YES)
//        {
//            [NSTimer scheduledTimerWithTimeInterval:2.0
//                                             target:self
//                                           selector:@selector(targetMethodForsingleUser)
//                                           userInfo:nil
//                                            repeats:NO];
//        }
//        else
//        {
//        [NSTimer scheduledTimerWithTimeInterval:2.0
//                                         target:self
//                                       selector:@selector(targetMethod)
//                                       userInfo:nil
//                                        repeats:NO];
//        }
//        
//    }
//    
//    }
//    
//}
//-(void)targetMethodForsingleUser
//{
//    SinglePlayerResultViewController *objSinglePlayerResultViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SinglePlayerResultViewController"];
//    // objSinglePlayerResultViewController.loginUser = self.loginUser;
//    objSinglePlayerResultViewController.FinalResponseArray = FinalResponse;
//    [self.navigationController pushViewController:objSinglePlayerResultViewController animated:YES];
//    
//}
//-(void)targetMethod
//{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    MultiplePlayerResultViewController *objMultiplePlayerResultViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"MultiplePlayerResultViewController"];
//    objMultiplePlayerResultViewController.FinalResponseArray =FinalResponse;
//    //objMultiplePlayerResultViewController.loginUser = self.loginUser;
//    [self.navigationController pushViewController:objMultiplePlayerResultViewController animated:YES];
//    
//}


@end
