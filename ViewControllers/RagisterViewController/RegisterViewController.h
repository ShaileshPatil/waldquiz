//
//  RegisterViewController.h
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "IIViewDeckController.h"

@interface RegisterViewController : GAITrackedViewController<NSURLConnectionDataDelegate,NSURLConnectionDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *surnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordResetTextField;
@property (strong, nonatomic) IBOutlet UIButton *ragistrationAction;
- (IBAction)ragistrationActionPerformed:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *registrationScrollView;




@end
