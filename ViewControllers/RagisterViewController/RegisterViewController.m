//
//  RegisterViewController.m
//  QuizApp
//
//  Created by Shailendra on 14/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "HomeViewController.h"
//#import "KeychainItemWrapper.h"


@interface RegisterViewController ()
{
    BOOL final;
    NSMutableData *receivedData;

}

@end

@implementation RegisterViewController


NSString *const kXMPPmyJID = @"kXMPPmyJID";
NSString *const kXMPPmyPassword = @"kXMPPmyPassword";
#pragma mark-  ViewLifeCycleMethods

- (void)viewDidLoad
{
    [super viewDidLoad];
   [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Registrieren";
     //self.navigationItem.title = @"REGISTRIEREN";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"RegisterViewController";
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(refreshPropertyList)];
    self.navigationItem.leftBarButtonItem = anotherButton;
    
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"TopChatLogin" accessGroup:nil];
//    
//    if([keychainItem objectForKey:(__bridge id)(kSecValueData)] && ![[keychainItem objectForKey:(__bridge id)(kSecValueData)] isEqualToString:@""])
//    {
//        self.emailTextField.text = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyJID];
//        
//        [self setField:self.emailTextField forKey:kXMPPmyJID];
//        [self setPassword: self.passwordTextField.text forKey:kXMPPmyPassword];
//       
//        
//    }
    

}
//- (void)setField:(UITextField *)field forKey:(NSString *)key
//{
//    if (field.text != nil)
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:field.text forKey:key];
//    } else {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
//    }
//}
//- (void)setPassword:(NSString *)field forKey:(NSString *)key
//{
//    if (field != nil)
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:field forKey:key];
//    } else {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
//    }
//}

-(void)refreshPropertyList
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-  CustomMethods
-(BOOL)email:(NSString *)mail
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:mail];
    return YES;
    
    
    //    return [self.emailTextField.text containsString:@"redbytes.in"];
}
-(BOOL)name:(NSString *)name
{
    
   NSString *emailRegex = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:name];
    
    
    
    //    return [self.emailTextField.text containsString:@"redbytes.in"];
}

#pragma mark-  textFieldDelegateMethods
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.tag == 1)
    {
        BOOL check;
        check = [self name:self.nameTextField.text];
        if (check == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Gerben Sie gultige Namen" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
            self.nameTextField.text=@"";
            alert.tag= 2;
            [alert show];
//            UITextField *textFiled1 =(UITextField*) [self.view viewWithTag:1];
//            check = YES;
//            [textFiled1 resignFirstResponder];

        }
        else
        {
            if (self.nameTextField.text.length >= 3)
            {
            }

            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Name sollte nicht weniger als 2 zeichen lang sein" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
                alert.tag= 2;
                [alert show];
                
                
                
            }

            
        }
       
        
        
    }
    if (textField.tag == 3)
    {
        
        
            BOOL succes;
            succes=[self email:self.emailTextField.text];
            if (succes==NO)
            {
                
                self.emailTextField.placeholder=@"Email-id";
                if (self.emailTextField.text.length>0)
                {
                    //[self.emailErrorLabel setHidden:FALSE];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte tippen Sie korrekte E-Mail-" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
                    
                    [alert show];
                     alert.tag= 4;
                    UITextField *textFiled3 =(UITextField*) [self.view viewWithTag:3];
                    
                    [textFiled3 resignFirstResponder];
                }
                self.emailTextField.text=@"";
                final=NO;
                
            }

    }
    
    
    
        return YES;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    UITextPosition *beginning = [textField beginningOfDocument];
//    [textField setSelectedTextRange:[textField textRangeFromPosition:beginning
//                                                          toPosition:beginning]];
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [self.registrationScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}
-(void)tapAction:(UITapGestureRecognizer *)tap
{
    UITextField *textFiled1 =(UITextField*) [self.view viewWithTag:1];
    [textFiled1 resignFirstResponder];
    UITextField *textFiled2 =(UITextField*) [self.view viewWithTag:2];
    [textFiled2 resignFirstResponder];
    UITextField *textFiled3 =(UITextField*) [self.view viewWithTag:3];
    
    [textFiled3 resignFirstResponder];
    UITextField *textFiled4 =(UITextField*) [self.view viewWithTag:4];
    
    [textFiled4 resignFirstResponder];    [self.view removeGestureRecognizer:tap];
    UITextField *textFiled5 =(UITextField*) [self.view viewWithTag:5];
    
    [textFiled5 resignFirstResponder];    [self.view removeGestureRecognizer:tap];
    [self.registrationScrollView setContentOffset:CGPointMake(0,0) animated:YES];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
   
    [self.registrationScrollView setContentOffset:CGPointMake(0,50) animated:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(tapAction:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:tap];
    //self.nameTextField.text.length >= 3
    
    if (textField.tag == 3) {
        BOOL check = [self name:self.surnameTextField.text];
        if (check == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte geben Sie eine galtige familienname" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
            self.surnameTextField.text=@"";
            alert.tag= 3;
            [alert show];
            //            UITextField *textFiled1 =(UITextField*) [self.view viewWithTag:1];
            //            check = YES;
            //            [textFiled1 resignFirstResponder];
            
        }
    }
    return YES;
    
    
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.nameTextField )
    {
        
        if (string.length != 0)
        {
            if (textField.text.length >= 20)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Zeichenbegrenzung überquert" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alert show];
                return NO;
            }
        }
    }
    return YES;
    

   
}
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if (textField.tag == 5)
//    {
//        if ([self.passwordTextField.text isEqualToString:self.passwordResetTextField.text])
//        {
//            
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Please Type Correct Password" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
//            [alert show];
//            
//        }
//    }
//}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//   
//    int MAX_LENGTH = 20;
//    int MIN_LENGTH = 3;
//    
//    NSInteger insertDelta = string.length - range.length;
//    
//    if (self.nameTextField.text.length + insertDelta > MAX_LENGTH && self.nameTextField.text.length + insertDelta > MIN_LENGTH)
//    {
//        return NO; // the new string would be longer than MAX_LENGTH
//    }
//    else
//    {
//        return YES;
//    }
//
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ragistrationActionPerformed:(id)sender
{
    [self.view endEditing:YES];
    
    if (self.nameTextField.text.length >=3)
    {
      if([self.passwordTextField.text isEqualToString:self.passwordResetTextField.text])
        {
             [self serverCommunication];
        }
    
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Bitte Passwort bestätigen" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
            [alert show];
            self.passwordResetTextField.text = @"";
        }

    
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"Name muß größer als 2" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
        [alert show];
        self.nameTextField.text = @"";

        
    }
    
   
}
#pragma mark-  connectionMethods
-(void)serverCommunication
{
    
    
    if (self.nameTextField.text.length>0&&self.surnameTextField.text .length>0&&self.emailTextField.text.length>0&&self.passwordTextField.text.length>0&&self.passwordResetTextField.text.length>0)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *post = [NSString stringWithFormat:@"first_name=%@&last_name=%@&email=%@&password=%@",self.nameTextField.text,self.surnameTextField.text,self.emailTextField.text,self.passwordTextField.text];
         NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
         NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
         NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
         
         [request setURL:[NSURL URLWithString:@"http://server51.cyon.ch/~waldquiz/rest/api.php?action=userRegistration"]];
         [request setHTTPMethod:@"POST"];
         [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
         [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
         [request setHTTPBody:postData];
         
         NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        if (connection)
        {
             NSLog(@"Connection Successful");
        }
        else
        {
            UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Verbindung nicht hergestellt werden konnte"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [connectFailMessage show];
        }

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@" Bitte geben Sie alle Felder aus" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark-  connectionDelegateMethods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    receivedData = [NSMutableData data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSLog(@"response data - %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    NSError* error;
    id json = [NSJSONSerialization JSONObjectWithData:receivedData
                                                         options:kNilOptions
                                                           error:&error];
    
    if ([json isKindOfClass:[NSDictionary class]])
    {
    NSString* responseError = [json valueForKey:@"error"];
    
    NSLog(@"error: %@", responseError);
        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectFailMessage show];
//        self.nameTextField.text = @"";
//        self.surnameTextField.text = @"";
//        self.emailTextField.text = @"";
//        self.passwordTextField.text=@"";
//        self.passwordResetTextField.text=@"";

    }
    
        
    
    else if ([json isKindOfClass:[NSArray class]] )
    {
       

       NSArray *array = json;
   NSLog(@"%@",array);
    UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Warnmeldung " message:@"Benutzer erfolgreich registriert"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [connectSuccessMessage setTag:1];
    [connectSuccessMessage show];
        
//        self.nameTextField.text = @"";
//        self.surnameTextField.text = @"";
//        self.emailTextField.text = @"";
//        self.passwordTextField.text=@"";
//        self.passwordResetTextField.text=@"";
    
        
    }

    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (buttonIndex == 0)
        {
            NSLog(@"Cancel Tapped.");
            [NSTimer scheduledTimerWithTimeInterval:2.0
                                             target:self
                                           selector:@selector(targetMethod)
                                           userInfo:nil
                                            repeats:NO];
            
        }
        
    }
   else if (alertView.tag==2)
    {
         self.nameTextField.text = @"";
        [self.view endEditing:YES];
        [self.nameTextField becomeFirstResponder];
    }
   else if (alertView.tag==3)
   {
       self.surnameTextField.text = @"";
       [self.view endEditing:YES];
       [self.surnameTextField becomeFirstResponder];
   }
   else if (alertView.tag==4)
   {
       self.emailTextField.text = @"";
       [self.view endEditing:YES];
       [self.emailTextField becomeFirstResponder];
   }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
   
}
-(void)targetMethod
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    LoginViewController *objLoginViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objLoginViewController animated:YES];
    
//    HomeViewController *objHomeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    //objHomeViewController.loginUser =array;
//    //        [self.navigationController pushViewController:objHomeViewController animated:YES];
//    UIStoryboard *storiboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *menuViewController = [storiboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
//    
//    [self.navigationController setNavigationBarHidden:NO];
//    self.viewDeckController.rightController = menuViewController;
//    [[self navigationController] setViewControllers:[NSArray arrayWithObject:objHomeViewController]  animated:NO];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSLog(@"response data with Failure- %@", [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnmeldung" message:@"erfolglos registriert" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:@"Cancel", nil];
    [alert show];
    
}

@end
