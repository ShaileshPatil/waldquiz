//
//  HomeViewController.m
//  QuizApp
//
//  Created by Shailendra on 28/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "HomeViewController.h"
#import "CustomTableViewCell.h"
#import "NewGameViewController.h"
#import "RankViewController.h"
#import "AsyncImageView.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "ConncetionViewController.h"
#import "QuestionViewController.h"
#import "QuestionWithImageViewController.h"
#import "MBProgressHUD.h"
#import "CustomLabelViewCell.h"


@interface HomeViewController ()

{
    UILabel *labelObject;
    NSMutableArray *giveChalangedArray,*takechallengesArray;
    UIView *SectionFooterView;
    UILabel *footerLabel;
    NSArray *array;
    AppDelegate *objAppDelegate;
     NSString *User_id;
    
    NSString *urlString;
    ConncetionViewController *objConncetion;
     UIViewController *nextView;
    

}

@end

@implementation HomeViewController
//@synthesize loginUser;
#pragma mark-  ViewLifeCycleMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    objAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
      objAppDelegate.tagArray =[[NSMutableArray alloc] init];
    [self.navigationController.navigationBar setHidden:NO];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1]];
    
    [self.challengeTableView setBackgroundColor:[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1]];


    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    //self.navigationItem.title = @"HOME";
     self.navigationItem.title = @"Home";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]
                                                                    };
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    giveChalangedArray=[[NSMutableArray alloc]init];
    

    takechallengesArray=[[NSMutableArray alloc]init];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    closeButton.frame = CGRectMake(0, 0, 30, 24);
    [closeButton setImage:[UIImage imageNamed:@"menu icon.png"] forState:UIControlStateNormal];
    
//    [closeButton addTarget:self action:@selector(closenew:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton addTarget:self.viewDeckController action:@selector(toggleRightView)
         forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *btnclose =[[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    self.navigationItem.rightBarButtonItem = btnclose;
    //user = objAppDelegate.loginUser;
    
    // Do any additional setup after loading the view.
    [[NSUserDefaults standardUserDefaults]setValue:@"No" forKey:@"Success"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    self.view.backgroundColor =[UIColor colorWithRed:236/255.0 green:222/255.0 blue:161/255.0 alpha:1];
//    if (objAppDelegate.hidebackButton == YES)
//    {
//        self.navigationItem.hidesBackButton = YES;
//        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"< ZURÜCK" style:UIBarButtonItemStylePlain
//                                                                         target:self action:@selector(refreshPropertyList)];
//        self.navigationItem.leftBarButtonItem = anotherButton;
//        objAppDelegate.hidebackButton = NO;
//    }
    
}

//-(void)refreshPropertyList
//{
//    ViewController *objViewController= [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"ViewController"];
//    //objHomeViewController.loginUser =user;
//    [self.viewDeckController closeRightViewBouncing:^(IIViewDeckController *controller)
//     {
//         
//         [((UINavigationController *)controller.centerController) setNavigationBarHidden:NO  animated:NO];
//         
//         
//         [((UINavigationController *)controller.centerController) setViewControllers:[NSArray arrayWithObject:objViewController]  animated:NO];
//     }];
//
//    
//}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"HomeViewController";
      self.button1.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
      self.rankButtonClicked.backgroundColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
    
    NSArray *loginUser = [[NSUserDefaults standardUserDefaults]
                          valueForKey:@"loginUser"];
    objConncetion = [[ConncetionViewController alloc]init];
    objConncetion.delegate = self;
    if (loginUser != nil)
    {
        objAppDelegate.loginUser = loginUser;
        User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];
       

    }
    else
    {
       User_id =[[objAppDelegate.loginUser objectAtIndex:0]valueForKey:@"uid"];
    }
    
       urlString = @"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getChallenges";

    [objConncetion serverCommunicatio:urlString input:User_id];
    
    [super viewWillAppear:animated];
    self.screenName = @"HomeViewController";
    
    if (nextView != nil) {
        _currentIndex ++;
        if ([[[array objectAtIndex:_currentIndex]valueForKey:@"question_photo"]isEqualToString:@""] )
        {
            QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
            objQuestionViewController.questionsArray =array;
            objQuestionViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionViewController animated:YES];
            
        }
        else
        {
            QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
            objQuestionWithImageViewController.questionsArrayWithImage =array;
            objQuestionWithImageViewController.currentIndex = _currentIndex;
            [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
            
            
        }
        
    }


}
-(IBAction)closenew:(id)sender
{
    [[self slidingViewController] anchorTopViewTo:ADAnchorSideLeft];
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-
#pragma mark-  connectionDelegateMethods

-(void)receiveData:(id)dict
{
    if ([dict isKindOfClass:[NSDictionary class]])
    {
        //NSString* responseError = [dict valueForKey:@"msg"];
        takechallengesArray = [dict valueForKey:@"receivedChallenges"];
        giveChalangedArray =[dict valueForKey:@"givenChallenges"];
        
//        UIAlertView *connectFailMessage = [[UIAlertView alloc] initWithTitle:@"Get Challanges " message:responseError delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [connectFailMessage show];
        
    }
    
    else if ([dict isKindOfClass:[NSArray class]] )
    {
        NSLog(@"NSArray");
        array = dict;
        
        [objAppDelegate.tagArray removeAllObjects];
        
        _currentIndex = 0;
        if ([[[array objectAtIndex:0]valueForKey:@"question_photo"]isEqualToString:@""])
        {
            QuestionViewController *objQuestionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionViewController"];
            objQuestionViewController.questionsArray =array;
            objQuestionViewController.currentIndex = 0;
            //objQuestionViewController.loginUser = self.loginUser;
            nextView = objQuestionViewController;
            [self.navigationController pushViewController:objQuestionViewController animated:YES];
            
        }
        else
        {
            QuestionWithImageViewController *objQuestionWithImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"QuestionWithImageViewController"];
            objQuestionWithImageViewController.questionsArrayWithImage =array;
            objQuestionWithImageViewController.currentIndex = 0;
            //objQuestionWithImageViewController.loginUser = self.loginUser;
            nextView = objQuestionWithImageViewController;
            [self.navigationController pushViewController:objQuestionWithImageViewController animated:YES];
            
            
        }

        
        //        UIAlertView *connectSuccessMessage = [[UIAlertView alloc] initWithTitle:@"Get Challanges " message:@"Questions For Test"  delegate: self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //
        //        [connectSuccessMessage show];
        
        
        
    }
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Success"] isEqualToString:@"YES"])
    {
        
    }
    else
    {
       [self.challengeTableView reloadData];
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-  buttonActionMethods
- (IBAction)NewGameButtonClicked:(id)sender
{
    NewGameViewController *objNewGameViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"NewGameViewController"];
    //objNewGameViewController.loginUser =user;
        [self.navigationController pushViewController:objNewGameViewController animated:YES];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ergebnis" message:@"In Entwicklung" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
//    [alert show];
}

- (IBAction)ShowRankActionPerformed:(id)sender
{
    RankViewController *objRankViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"RankViewController"];
       
        [self.navigationController pushViewController:objRankViewController animated:YES];

//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ergebnis" message:@"In Entwicklung" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
//    [alert show];
}
#pragma mark-  TableViewDatasourseAndDelegateMethods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView;
    UILabel *headerLabel;
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 15.0 ];
    headerLabel.font  = myFont;
    switch (section)
    {
        case 0:
            
        {
            
        sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
            sectionHeaderView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];

            
            headerLabel = [[UILabel alloc] initWithFrame:
                           CGRectMake(0, 5, sectionHeaderView.frame.size.width, 25.0)];
            
            headerLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];

            headerLabel.textAlignment = NSTextAlignmentLeft;
            [headerLabel setFont:[UIFont fontWithName:@"Verdana" size:15.0]];
            [sectionHeaderView addSubview:headerLabel];
            
            headerLabel.text = @"Herausforderungen";
           headerLabel.textColor = [UIColor blackColor];
            return sectionHeaderView;

                    }
            break;

        case 1:
       
        {
            sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
            sectionHeaderView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];

            
            headerLabel = [[UILabel alloc] initWithFrame:
                           CGRectMake(0, 5, sectionHeaderView.frame.size.width, 25.0)];
            
            headerLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];

            headerLabel.textAlignment = NSTextAlignmentLeft;
            [headerLabel setFont:[UIFont fontWithName:@"Verdana" size:15.0]];
            [sectionHeaderView addSubview:headerLabel];
            
            headerLabel.text = @"Herausgeforderte";
            headerLabel.textColor = [UIColor blackColor];
            return sectionHeaderView;
            
            
        }
            
            break;
            default:
            break;
    }
    
    return sectionHeaderView;
    
    
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 15.0 ];
//    footerLabel.font  = myFont;
//    switch (section)
//    {
//        case 0:
//            
//            SectionFooterView = [[UIView alloc] initWithFrame:
//                                 CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
//            SectionFooterView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
//            
//            
//            footerLabel = [[UILabel alloc] initWithFrame:
//                           CGRectMake(0, 5, SectionFooterView.frame.size.width, 25.0)];
//            
//            footerLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
//            
//            footerLabel.textAlignment = NSTextAlignmentCenter;
//            [footerLabel setFont:[UIFont fontWithName:@"Verdana" size:15.0]];
//            [SectionFooterView addSubview:footerLabel];
//            
//            //footerLabel.text = @"MEHR ANZEIGEN";
//            footerLabel.textColor =[UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
//            return SectionFooterView;
//            
//            break;
//        case 1:
//            SectionFooterView = [[UIView alloc] initWithFrame:
//                                 CGRectMake(0, 0, tableView.frame.size.width, 10.0)];
//            SectionFooterView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
//            
//            
//            footerLabel = [[UILabel alloc] initWithFrame:
//                           CGRectMake(0, 5, SectionFooterView.frame.size.width, 25.0)];
//            
//            footerLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
//            
//            footerLabel.textAlignment = NSTextAlignmentCenter;
//            [footerLabel setFont:[UIFont fontWithName:@"Verdana" size:15.0]];
//            [SectionFooterView addSubview:footerLabel];
//            
//            //footerLabel.text = @"MEHR ANZEIGEN";
//            footerLabel.textColor = [UIColor colorWithRed:31/255.0 green:155/255.0 blue:48/255.0 alpha:1];
//            return SectionFooterView;
//            
//            break;
//        default:
//            break;
//    }
//    
//    return [[UIView alloc] initWithFrame:CGRectZero];
//
//    
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
   
    if(section==0)
    {
        if (takechallengesArray == (id)[NSNull null])
        {
            return 1;
            
        }
        else
        {
        return takechallengesArray.count;
        }
        
    }
    if(section==1)
    {
        if (giveChalangedArray == (id)[NSNull null])
        {
          return 1;
        }
        else
        {
             return giveChalangedArray.count;
            
        }
       
        
    }
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
  //  static NSString *cellIdentifier = @"Cell";
    
    
    //static NSString *CellIdentifier = @"Cell";
    
    //static NSString *CellIdentifier1 = @"Cell1";
    CustomLabelViewCell *cell1;
    
    CustomTableViewCell *cell;
    
    
    if (indexPath.section==0)
    {
        
        if (takechallengesArray == (id)[NSNull null])
        
        {
            //UILabel *labelObject;
            
            cell1 = (CustomLabelViewCell *)[tableView dequeueReusableCellWithIdentifier:nil];
              cell1.contentView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
            if (cell1 == nil)
            {
                NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"CustomLabelViewCell" owner:self options:nil];
                cell1=(CustomLabelViewCell *) [nibbb objectAtIndex:0];
                
                
                
            }
            cell1.customLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
            cell1.customLabel.text =@"Du hast noch niemanden herausgefordert";
            
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return cell1;
            
            
        }
        else
        {
            cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            
            if (cell == nil)
            {
                NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"CustomTableViewCell" owner:self options:nil];
                cell=(CustomTableViewCell *) [nibbb objectAtIndex:0];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            NSString *name =[[takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"first_name"];
            NSString *surname =[[takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"last_name"];
            NSString *allname = [name stringByAppendingString:surname];
            cell.customNameLabel.text = allname;
            cell.imageView.backgroundColor =[UIColor greenColor];
            if ([[takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"image"] == (id)[NSNull null])
            {
                NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
                cell.cutomImageview.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
                //cell.cutomImageview.image = [UIImage imageNamed:@"chat.png"];

            }
            else
            {
                cell.cutomImageview.imageURL =[NSURL URLWithString:(NSString *)[[takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"image"]];
            }
            cell.countLabel.text =[[takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"status"];
            
             return cell;

            
        }
        
        
    }
    else if (indexPath.section==1)
    {

        if (giveChalangedArray == (id)[NSNull null])
            
        {
            
           cell1 = (CustomLabelViewCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            cell1.contentView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];            
            if (cell1 == nil)
            {
                NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"CustomLabelViewCell" owner:self options:nil];
                cell1=(CustomLabelViewCell *) [nibbb objectAtIndex:0];
                
                
                
            }
            cell1.customLabel.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:162/255.0 alpha:1];
            cell1.customLabel.text =@"Du hast noch niemanden herausgefordert";
            
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell1;

        }
        else
        {
            cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            
            if (cell == nil)
            {
                NSArray *nibbb=[[NSBundle mainBundle] loadNibNamed:@"CustomTableViewCell" owner:self options:nil];
                cell=(CustomTableViewCell *) [nibbb objectAtIndex:0];
                
                
                
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
           // NSString *name =[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"name"];
//            NSString *surname =[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"last_name"];
//            NSString *allname = [name stringByAppendingString:surname];
            if ([[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"name"] isEqualToString:@" "] || [[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"name"] isEqualToString:@""])
            {
                cell.customNameLabel.text = @"No Name";
            }
            else
            {
            cell.customNameLabel.text =[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"name"];
            }
            cell.imageView.backgroundColor =[UIColor greenColor];
            if ([[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"image"] == (id)[NSNull null])
            {
                NSString *defaultUrl = @"https://www.waldquiz.ch/uploads/profilephoto/default_profile.jpg";
                cell.cutomImageview.imageURL =[NSURL URLWithString:(NSString *)defaultUrl];
                //cell.cutomImageview.image = [UIImage imageNamed:@"chat.png"];
                
            }
            else
            {
                cell.cutomImageview.imageURL =[NSURL URLWithString:(NSString *)[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"image"]];
            }
            
            
            if ([[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"gameStatus"]  isEqualToString: @"Gewonnen"] ||[[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"gameStatus"]  isEqualToString: @"Verloren"] )
            {
                if ([[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"winner"]  isEqualToString: User_id])
                {
                      cell.countLabel.text =@"Verloren";
                    cell.contentView.backgroundColor = [UIColor colorWithRed:186/255.0 green:42/255.0 blue:33/255.0 alpha:1];
                    
                }else{
                    cell.countLabel.text =@"Gewonnen";
                     cell.contentView.backgroundColor = [UIColor colorWithRed:29/255.0 green:149/255.0 blue:42/255.0 alpha:1];
                }
                
            }else{
                cell.countLabel.text =[[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"gameStatus"];
            }
            
            
                       return cell;
            
            
        }

    }
        
    // Configure the cell...
    
    return cell;
}
-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    
//    if(section==0)
//    {
//        return 25.0f;
//        
//    }
//    if(section==1)
//    {
//        return 25.0f;
//        
//    }
    return 0;

    
}

//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
//{
//    
//    if(section==0)
//    {
//        return @"       MEHR ANZEIGEN ";
//    }
//    if(section==1)
//    {
//       return @"        MEHR ANZEIGEN ";
//        
//    }
// 
//    return @"MEHR ANZEIGEN ";
//
//    
//}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 25.0f;
        
    }
    if(section==1)
    {
        return 25.0f;
        
    }
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section==0)
    {
        if (takechallengesArray == (id)[NSNull null])
        {
            
        }
        else
        {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"YES" forKey:@"Success"];
        [[NSUserDefaults standardUserDefaults]synchronize];
     User_id=  [ [takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"challenge_id"];
        urlString = @"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getSameQuestionsByChallengeId";
        [objConncetion serverCommunicatio:urlString input:User_id];
       
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:[ [takechallengesArray objectAtIndex:indexPath.row]valueForKey:@"challenge_id"]];
        objAppDelegate.challangeId = myNumber;

        
        }
        
    }
    if (indexPath.section == 1)
    {
//      User_id=   [[giveChalangedArray objectAtIndex:indexPath.row]valueForKey:@"challenge_id"];
//         urlString = @"http://server51.cyon.ch/~waldquiz/rest/api.php?action=getSameQuestionsByChallengeId";
       

        
    }
}

@end
