//
//  HomeViewController.h
//  QuizApp
//
//  Created by Shailendra on 28/04/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADSlidingViewController.h"
#import "IIViewDeckController.h"
#import "GAITrackedViewController.h"
#import "ConncetionViewController.h"

@interface HomeViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,COnnectionProtocol>
- (IBAction)NewGameButtonClicked:(id)sender;
- (IBAction)ShowRankActionPerformed:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *challengeTableView;
@property(nonatomic, assign) int currentIndex;
//@property (strong, nonatomic) IBOutlet NSArray *loginUser;
@property (weak, nonatomic) IBOutlet UIButton *button1;

@property (weak, nonatomic) IBOutlet UIButton *rankButtonClicked;

@end
