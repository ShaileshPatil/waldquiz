//
//  ConncetionViewController.h
//  QuizApp
//
//  Created by Shailendra on 29/05/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol COnnectionProtocol<NSObject>

-(void)receiveData:(id)dict;


@end

@interface ConncetionViewController : UIViewController
@property(strong,nonatomic)id<COnnectionProtocol>delegate;
-(void)serverCommunicatio:(NSString *)url input:(NSString *)User_id;
@end
